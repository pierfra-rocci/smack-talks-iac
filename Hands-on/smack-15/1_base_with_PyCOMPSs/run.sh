######################################################
# Run sampling with PyCOMPSs example job             #
######################################################
#                                                    #
# Usage sample:                                      #
#                                                    #
#    ./run.sh                                        #
#                                                    #
# CAUTION! Do not run in the LaPalma login node      #
######################################################

pycompss run \
  --log_level=off \
  --graph \
  --tracing \
  sampling_pycompss.py
