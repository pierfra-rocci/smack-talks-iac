import time
import numpy as np
import emcee


def execution_params():
    """Define execution parameters."""
    np.random.seed(42)
    initial = np.random.randn(32, 5)
    nwalkers, ndim = initial.shape
    nsteps = 10
    return initial, nwalkers, ndim, nsteps


def log_prob(theta):
    """Sampling function to apply."""
    time.sleep(0.2)  # Computation load simulation
    return -0.5 * np.sum(theta**2)


def emcee_sequential(params):
    """emcee sequential usage."""
    initial, nwalkers, ndim, nsteps = params
    sampler = emcee.EnsembleSampler(nwalkers, ndim, log_prob)
    start = time.time()
    result = sampler.run_mcmc(initial, nsteps, progress=True)
    end = time.time()
    print("Serial took {0:.1f} seconds".format(end - start))
    return result


###########################################################
# MAIN FUNCTON
###########################################################

if __name__ == "__main__":
    params = execution_params()
    result_seq = emcee_sequential(params)
