######################################################
# Submit sampling example job                        #
######################################################
#                                                    #
# Usage sample:                                      #
#                                                    #
#    ./launch.sh                                     #
#                                                    #
# Wait for the job to be executed and then you can   #
# check the job out and err files.                   #
######################################################

module load python/3.7.4

sbatch job.sbatch
