Programming parallel workloads with PyCOMPSs in La Palma
========================================================

HANDS-ON
--------

Contents
~~~~~~~~

This hands-on includes three examples:

* 0_base:

  Sample exercise showing how to use **emcee sequentially**.

* 1_base_with_PyCOMPSs

  Sample exercise showing how to use **emcee parallelized with PyCOMPSs**.

* 2_comparison

  Sample exercise running a larger execution **comparing emcee sequentially
  against parallelized with PyCOMPSs**.


Instructions
~~~~~~~~~~~~

1. Copy this folder into the LaPalma HPC.

2. Run the examples with the `launch.sh` script provided within each folder.

   .. code-block:: bash

     ./launch.sh

   **NOTE:** The ``launch.sh`` script contains the ``--reservation`` flag with the
   reservation that will be active during the training session. Please, remove
   or modify the reservation value if not in the training session.

3. Wait for the job to be executed (its status can be checked with `squeue`).

4. Check the out and err files.
