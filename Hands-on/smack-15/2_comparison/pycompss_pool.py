"""Helper module that provides the map interface for PyCOMPSs."""

from pycompss.api.api import compss_wait_on


def map(func, iterable, chunksize=None):
    """Map function interface for PyCOMPSs.

    :param func: Function to apply to all iterable elements.
    :param iterable: Iterable of elements.
    :param chunksize: Chunk size (Not supported in this implementation).
    :returns: The result of applying func over all iterable elements.
    """
    result = []
    for i in iterable:
        result.append(func(i))
    result = compss_wait_on(result)
    return result
