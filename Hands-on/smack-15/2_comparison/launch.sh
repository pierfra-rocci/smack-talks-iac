######################################################
# Submit comparison example job                      #
######################################################
#                                                    #
# Usage sample:                                      #
#                                                    #
#    ./launch.sh                                     #
#                                                    #
# Wait for the job to be executed and then you can   #
# check the job out and err files.                   #
######################################################

module load compss/2.10.1

pycompss job submit \
  --qos=debug \
  --log_level=off \
  --job_name=compare_sampling \
  --exec_time=20 \
  --num_nodes=2 \
  --reservation=bsc19_113 \
  compare.py
