SMACK 4: Shell scripting for automation and text editors
--------------------------------------------------------

### Abstract

In the previous SMACKs, basic operations on the command-line were
reviewed in interactive mode: where you enter one command, wait for it
to finish and enter the next command. The shell's interactive mode is
good for small/fast tests, but is not scalable. For example when you
know the commands and parameters work and want to apply them to
hundreds/thousands of targets. Shell scripts are simply plain-text
files that store all the various commands that you want to be executed
in sequence. They are designed precisely for the easier management of
operations that are more complex than a simple command. In fact, many
of the commands in the Unix-like operating systems are actually shell
scripts! Here will review some basic intricacies with designing robust
shell scripts and avoiding common pitfalls. Also, since shell scripts
are simple plain-text files, we will also do a short review of simple
plain-text editors like GNU Nano and more advanced editors like Vim
and GNU Emacs. Advanced editors have many useful features to simplify
programming in many languages (including shell programming) and don't
need complex graphic environments so they can be run in the raw
command-line (a major advantage when scaling your project up to
supercomputers that don't have a graphic environment). For newcomers
to data-intensive astronomical research, we encourage you to select an
advanced editor, and master it early to greatly simply your research
(in any language).

Relevant links:
* Lecture notes: https://gitlab.com/makhlaghi/smack-talks-iac/-/blob/master/smack-4-scripting.md
* Video of this tutorial: https://www.youtube.com/watch?v=g7jJwikuebc
* Gnuastro scripting tutorial: https://www.gnu.org/software/gnuastro/manual/html_node/Writing-scripts-to-automate-the-steps.html
* Vim tips wiki: https://vim.fandom.com/wiki/Vim_Tips_Wiki

Working scenario prepared by Mohammad Akhlaghi and Raul Infante-Sainz




### Working scenario

1. To show scripting, first we need a set of commands for an
   operation. So let's use Gnuastro (introduced in SMACK 3) for the
   simple job of selecting a bright and red galaxy from the [ABYSS
   HUDF WFC3/IR project](http://research.iac.es/proyecto/abyss). Here
   are the full commands we will be working with in this
   tutorial. This is usually how projects progress: you start by an
   interactive testing to see if an idea works, then you automate it
   by putting the commands in a script. So, please run them once on
   your computer (we will be using your executed commands in the next
   few steps).

   ```shell
   # Build the directory for downloaded files.
   mkdir download

   # Download the necessary images.
   url=http://cdsarc.u-strasbg.fr/ftp/J/A+A/621/A133/fits
   curl -o download/abyss-f105w.fits $url/ah_f105w.fits
   curl -o download/abyss-f125w.fits $url/ah_f125w.fits
   curl -o download/abyss-f160w.fits $url/ah_f160w.fits

   # Have a look at the images (just to get a feeling of the data)
   ds9 download/abyss-f105w.fits -zscale -zoom to fit
   ds9 download/abyss-f125w.fits -zscale -zoom to fit
   ds9 download/abyss-f160w.fits -zscale -zoom to fit

   # Run NoiseChisel (to identify pixels with signal)
   astnoisechisel download/abyss-f105w.fits --output=f105w-det.fits
   astnoisechisel download/abyss-f160w.fits --output=f160w-det.fits

   # Run Segment (to identify sub-structure)
   astsegment f160w-det.fits --output=f160w-seg.fits

   # Generate a catalog using the F160W image as reference.
   astmkcatalog f160w-seg.fits --zeropoint=25.94 --output=f160w-cat.fits --ids --ra --dec --magnitude
   astmkcatalog f160w-seg.fits --zeropoint=26.27 --valuesfile=f105w-det.fits --output=f105w-cat.fits --ids --ra --dec --magnitude

   # Just to confirm that both tables/catalogs have the same size.
   astfits f105w-cat.fits
   astfits f160w-cat.fits

   # Take out the magnitude column from the F105W catalog and put it
   # into the F160W catalog, then delete the temporary file and fix
   # the names of the two magnitude columns.
   asttable f105w-cat.fits -cMAGNITUDE -of105w-mag.fits
   asttable f160w-cat.fits --catcolumn=f105w-mag.fits --output=both-cat.fits
   astfits both-cat.fits --update=TTYPE4,MAG-F160W --update=TTYPE5,MAG-F105W
   rm f105w-mag.fits

   # Extract the RA and Dec of the reddest object (with a ',' between
   # the RA and Dec) that is relatively bright magnitude (to be nicely
   # visible) and put it into the variable 'center', then check the
   # value.
   center=$(asttable both-cat.fits --range=MAG-F160W,-inf,27 -cRA,DEC,'arith MAG-F105W MAG-F160W -' | asttable  --sort=3 --descending --head=1 -c1,2 | xargs printf "%s,%s")
   echo $center

   # Crop out the two filters around this coordinate.
   astcrop download/abyss-f160w.fits --center=$center --width=10/3600 -of160w-crop.fits
   astcrop download/abyss-f125w.fits --center=$center --width=10/3600 -of125w-crop.fits
   astcrop download/abyss-f105w.fits --center=$center --width=10/3600 -of105w-crop.fits

   # Convert the crops to JPEG for easier viewing
   astconvertt f160w-crop.fits f125w-crop.fits f105w-crop.fits -o red.jpg --fluxlow=-0.01 --fluxhigh=0.01 -h1 -h1 -h1
   ```



1. You can see the "history" of your previously executed commands with
   the `history` command, try it out and you will see every command
   you entered (even before starting this tutorial.

   ```shell
   # Show last (usually 500) commands that were executed. Note that
   # each command has a counter
   history

   # If you only want the last 10 commands:
   history 10

   # To keep this history of your analysis for future reference, you
   # can re-direct it to a file (remember the `>` from SMACK 2?):
   history > finding-reddest-galaxy.txt
   ```



1. But the history is full of extra commands; for example the check
   commands above (like `echo` or `ds9` which you just used for a
   visual check). Also, mistaken commands (for example caused by
   typos) are recorded. Furthermore, due to all those command numbers,
   this raw recorded history can't be automatically run! So this way
   of recording your project is only good for a temporary phase (as
   long as you remember which command was a test, which one was for
   visualization and which was a typo!).



1. A better way to record your commands is by putting the finally used
   commands in a curated, plain-text file (which can also be executed
   by the shell automatically). Hence you will need to use a
   plain-text editor (or "editor" for short). To start with, let's use
   one of the simplest editors GNU Nano (called with the `nano`
   command). Nano prints some of the most used commands to control it
   at the bottom of the screen, they are reviewed below. By
   convention, shell scripts have the `.sh` suffix, although it is by
   no means mandatory.

   ```shell
   # Let's start modifying it with Nano, here are some notes
   # (after nano opens), try playing with them a little:
   #   - `^` is the "Ctrl" key.
   #   - `M` is the "Alt" key.
   #   - `^g`: help, press `q' to come back to the editing space.
   #   - `^w`: search.
   #   - `^\`: find-and-replace.
   #   - `M-u`: undo.
   #   - `^s`: save.
   #   - `^x`: save and exit.
   nano first-script.sh
   ```



1. Let's copy the last few commnds into `first-script.sh` (through the
   opened Nano interface). In particular copy these commands, then
   save the file and close it (by pressing `^s`, then `^X`).

   ```shell
   center=$(asttable both-cat.fits --range=MAG-F160W,-inf,27 -cRA,DEC,'arith MAG-F105W MAG-F160W -' | asttable  --sort=3 --descending --head=1 -c1,2 | xargs printf "%s,%s")
   astcrop download/abyss-f160w.fits --center=$center --width=10/3600 -of160w-crop.fits
   astcrop download/abyss-f125w.fits --center=$center --width=10/3600 -of125w-crop.fits
   astcrop download/abyss-f105w.fits --center=$center --width=10/3600 -of105w-crop.fits
   astconvertt f160w-crop.fits f125w-crop.fits f105w-crop.fits -o red.jpg --fluxlow=-0.01 --fluxhigh=0.01 -h1 -h1 -h1
   ```



1. You should now be back in the shell, and you can check the creation
   of the script with `ls` and its contents with `cat`:

   ```shell
   # See if 'first-script.sh' exists
   ls

   # Check its contents:
   cat first-script.sh
   ```



1. When the shell (we'll assume GNU Bash, which is the most common
   shell) is given a plain-text file, it will execute every line as
   separate commands, allowing you to automate your analysis. So,
   let's execute this small test script. But since it will be very
   fast, just to you see that it actually worked, let's remove the
   files it will create. After its complete, you can confirm that they
   are all re-made.

   ```shell
   rm *-crop.fits *.jpg
   bash first-script.sh
   ls
   ```


1. Automation can be even made easier! But to benefit from them, let's
   pause for a moment and review some basic concepts first. Every file
   has three types of permissions: Read (or `r`), Write (or `w`) and
   Execute (`x`) for three types of users: the "owner", a "group", and
   the root administrator. You see these with the "long" (or `-l')
   option to `ls` like below. Note how the `download` directory has
   the executable flag, but all other files don't have it. By default,
   most operating systems don't activate the executable flag.

   ```shell
   ls -l
   ```



1. But with the executable flag activated, we can execute our script
   automatically (without necessarily calling `bash` before it). To
   modify the permission flags of a file, you can use `chmod`. In
   particular, to make it executable, you can use `+x` (to remove it
   later, if you want, you can use `-x`):

   ```shell
   # Just to confirm that it doesn't have the executable flag.
   ls -l first-script.sh

   # Activate the executable flag for the shell script, and confirm:
   chmod +x first-script.sh
   ls -l first-script.sh
   ```



1. The script is now executable, so we can run it like this. Note that
   because your script is not installed in standard location, you need
   to tell the shell to look for it in the current directory by
   prefixing the script name with `./`.

   ```shell
   ./first-script.sh
   ```



1. By default, the running shell will assume that the script should be
   run with itself. But a more systematic way is to tell the shell
   what program should be used to run the script. For example, on
   another computer the default shell may not be GNU Bash, but your
   script may use special features of Bash that aren't present in the
   other computer's shell. Later, you may also be writing scripts in
   other languages, for example Perl or Python (so you need to tell
   the shell to use those programs to interpretting the script, not a
   shell). To do this, Unix-like operating systems have the
   [Shebang](https://en.wikipedia.org/wiki/Shebang_(Unix)) (also known
   as "sha-bang" or "hashbang"). In short, the first two characters
   (in the first line) of your script should be `#!`, followed by the
   full address of the program that should be used to run the
   script. In this case, the full address of GNU Bash is `/bin/bash`,
   so open `first-script.sh` with `nano` again and start it with this
   line:

   ```shell
   #!/bin/bash
   ```



1. You can always find the full address of any program with the
   `which` command, so to confirm that the shebang above is correct,
   try it out. Note that `/usr/bin/bash` is another common location of
   Bash. Even if the output of `which` is not `/bin/bash`, you can
   confirm that it exists there with `ls /bin/bash`, it has just found
   the one in `/usr/bin` before the one in `/bin`.

   ```shell
   # See where the `bash` program is installed.
   which bash

   # You can use this for any other program too, for example
   which astnoisechisel
   which astcrop
   which ls
   ```



1. One feature of Bash scripts is that they don't stop if one command
   fails (this is intentional and useful in some scenarios). You can
   see this by making one of the command fail intentionally! Using
   your favorite editor, open the script and change
   `download/abyss-f160w.fits` to `DOWNLOAD/abyss-f160w.fits`, then
   run the script again (after removing its outputs). From the printed
   outputs, you can see that even though the first `astcrop` command
   failed, the script continued with the next steps, ultimtely leading
   to a failure in `astconvertt` (because one of its necessary inputs
   isn't created).

   ```shell
   rm *-crop.fits *.jpg
   ./first-script.sh
   ```


1. To ensure that a Bash script fully stops immediately after an error
   occurs, you need to "set" the "error" flag in your shell script, or
   add the following line in the script (ideally, right after the
   shebang):

   ```shell
   set -e
   ```


1. Since your research is done through scripts like this (in any
   programming language), its good to publish your source codes with
   your papers. But to do that, readers/users of your script need to
   know how they can use it, and who to contact in case of
   problems. So its always a good habit to put your name (as the
   copyright owner) and year of authorship following a `Copyright (C)`
   statement and choose a license to for your code. Then put all of
   these at the top of the file (just under the shebang). Here is an
   example if you want to use the [GNU General Public
   License](https://www.gnu.org/licenses/quick-guide-gplv3.html)
   (GPL), just replace in the parts in `FULLCAPS`:

   ```shell
   #!/bin/sh
   #
   # PUT THE PURPOSE OF THIS SCRIPT AS A SINGLE LINE HERE.
   #
   # Copyright (C) YEAR FIRSTNAME FAMILYNAME <YOUR@EMAIL.ADDRESS>
   #
   # This program is free software: you can redistribute it and/or modify
   # it under the terms of the GNU General Public License as published by
   # the Free Software Foundation, either version 3 of the License, or
   # (at your option) any later version.
   #
   # This program is distributed in the hope that it will be useful,
   # but WITHOUT ANY WARRANTY; without even the implied warranty of
   # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   # GNU General Public License for more details.
   #
   # You should have received a copy of the GNU General Public License
   # along with this program.  If not, see <http://www.gnu.org/licenses/>.
   ```



1. GNU Nano is designed to be a very simple command-line editor. But
   as you progress, you will notice its limitations. So let's also
   introduce two other more advanced editors (that you will need a
   week or so to get accustomed to): [Vim](https://www.vim.org) and
   [GNU Emacs](https://www.gnu.org/software/emacs).



1. You can open a plain-text file using Vim like below (in the
   comments you can see some descriptions of the steps).

    ```shell
    # - Normal/Insert Mode (esc/i)
    # - Move without mouse (h j k l)
    # - Bigger movements (w b ctrl-f ctrl-b)
    # - Start and end of file (gg G)
    # - Copy/Paste (y/p)
    # - Visual selection (v and move)
    # - Visual selection by rows (V and move)
    # - Visual selection by columns (ctrl-v and move)
    # - Select text and format it (v, select, gq)
    # - Search: /PATTERN
    # - Search and replace: :%s/PATTERN/REPLACE/g
    # - Search and replace with confirmation: :%s/PATTERN/REPLACE/gc
    # - Open another file splitting vertically the screen (:vsp other-file.txt)
    # - Difference between files: vimdiff
    # - Its own tutorial: vimtutor
    vim first-script.sh
    ```



1. Nice feature of Vim to insert text by columns. Consider for example that you
   want to add `#` at the beginning of several lines in order to comment them.

    ```shell
    # Press Esc to enter 'command mode'
    # Use Ctrl+v to enter visual block mode
    # Move Up/Downto select the columns of text in the lines you want to comment.
    # Then hit Shift+i and type the text you want to insert (#).
    # Then hit Esc, wait 1 second and the inserted text will appear on every line.
    ```



1. GNU Emacs is also activated by calling it before the plain-text
   file's name. Note that if you are using a graphical user interface,
   Emacs may open its own separate window (outside the terminal). To
   make sure this doesn't happen and you can use emacs directly, use
   the no-window option (or `-nw`).

   ```shell
   # You can directly start modifying the file (`^` and `M` are like Nano).
   # - To save the file, press ^x, followed by ^s.
   # - Add more examples here.
   # - Opening multiple files
   # - Holding control to go between words.
   # - 'M-x shell' to open a shell script.
   # - 'M-x compile' to run a command.
   # ...
   emacs -nw first-script.sh
   ```

1. Avoid long lines (breaking up the commands).

1. Formatting block of text by cutting long lines automatically.

1. Show conditionals in order to not create or download already existing files.

1. Use loops to avoid text redundances.

1. Show how variables can help improve the script and customize it.

1. Improve the script, for example by separating stars
              (redshift 0) and galaxies.

1. Put the script in other directories, and discuss PATH.
