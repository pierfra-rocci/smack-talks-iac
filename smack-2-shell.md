SMACK 2: POSIX power tools: basic operations on the command-line
----------------------------------------------------------------

### Abstract

The shell (or command-line) is the most commonly used (for advanced
operations) interface to Unix-like operating systems.  In this session
we'll introduce some of the most commonly used command-line/shell
features and how they may be used in a hypothetical research project,
using real astronomical datasets.  The session plan is
[available on GitLab](https://gitlab.com/makhlaghi/smack-talks-iac/-/blob/master/smack-2-shell.md),
and has been used in a talk that has been [recorded on YouTube](https://www.youtube.com/watch?v=48r76WVkQUI).

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/48r76WVkQUI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>



### Working scenario

1. Start with downloading the Hubble Space Telescope (HST) [UVUDF
   survey](https://archive.stsci.edu/prepds/uvudf) catalog.  If you
   don't have Wget, try cURL instead and ignore the first and third
   commands, for the second, put `curl -o` instead of `wget -O`.
    ```shell
    # Get the file with its original name
    wget https://archive.stsci.edu/missions/hlsp/uvudf/v2.0/hlsp_uvudf_hst_wfc3-uvis_udf-epoch3_multi_v2.0_cat.fits

    # Set a name for the downloaded file with `-O' option.
    wget -O catalog-raw.fits https://archive.stsci.edu/missions/hlsp/uvudf/v2.0/hlsp_uvudf_hst_wfc3-uvis_udf-epoch3_multi_v2.0_cat.fits

    # Delete the original, long-named file.
    rm hlsp_uvudf_hst_wfc3-uvis_udf-epoch3_multi_v2.0_cat.fits
    ```

1. Convert it to text file with [Gnuastro's
   Table](https://www.gnu.org/software/gnuastro/manual/html_node/Invoking-asttable.html).
   In the [next talk](smack-3-gnuastro.md) we will introduce Gnuastro
   programs (like `asttable`), but is already available on IAC's
   computer network and also in many package managers. For example on
   Debian-based operating systems, you can do `sudo apt install
   gnuastro` to get it.
    ```shell
    asttable catalog-raw.fits --output=catalog-raw.txt
    ```

1. See the file contents with `cat`.  If you don't have Gnuastro, you
   can get this file [in IAC's
   FTP](ftp://ftp.ll.iac.es/out/catalog-raw.txt).
    ```shell
    cat catalog-raw.txt
    ```

1. There is a lot of outputs from the command above,
   Let's see only the very beginning of the catalog with `head`.
    ```shell
    # By default only the first 10 lines are shown
    head catalog-raw.txt

    # You can ask for any number of first lines, for example 5
    head -5 catalog-raw.txt

    # Or the first 98 lines (where we also see the first row's values,
    # there are many columns which may be shown in more than one line,
    # but its all one "line" in the file).
    head -98 catalog-raw.txt
    ```

1. If you want to see the end of the catalog `tail`.
    ```shell
    # Last 10 lines by default
    tail catalog-raw.txt

    # Last line
    tail -1 catalog-raw.txt
    ```

1. Redirecting **to write** into a file
    ```shell
    echo abc
    echo abc > demo-file.txt
    ls
    cat demo-file.txt
    ```

1. For blank values they are using `-99` and `99` which can cause
   problems in arithmetic operations. Let's replace all of them by
   `nan` using [SED](https://en.wikipedia.org/wiki/Sed), or the
   "String EDitor".
    ```shell
    # In SED, the expression -e's/XXX/YYY/g' means to replace XXX with YYY.
    # So with the command below, we replace `b' with `B'.
    sed 's/b/B/g' demo-file.txt

    # We can use the same structure to replace any `99' (with spaces
    # around it) with `nan'.
    sed 's/ 99 / nan /g' catalog-raw.txt

    # But to keep the results, we need to redirect the output to a file.
    sed 's/ 99 / nan /g' catalog-raw.txt > catalog.txt
    ls
    cat catalog.txt

    # Now we can replace the `-99's is well.
    # But to have multiple expressions, we should use `-e' before them.
    sed -e's/ 99 / nan /g' -e's/ -99 / nan /g' catalog-raw.txt > catalog.txt
    cat catalog.txt
    ```

1. Use [Grep](https://en.wikipedia.org/wiki/Grep) to select rows.
   Consider all lines starting with `#` and save it into another file
   `header.txt`.  Same but saving only data by inverting the pattern
   match with `-v`.
    ```shell
    # Save lines that start with (^) a `#' in header.txt
    grep '^#' catalog.txt
    grep '^#' catalog.txt > header.txt
    cat header.txt

    # With `-v' we reverse the condition, so the data are printed.
    grep -v '^#' catalog.txt > data.txt
    ```

1. Also use Grep to find the column number *visually* based on
   name. As you will see in the [next talk](smack-3-gnuastro.md) with
   [Gnuastro's
   Table](https://www.gnu.org/software/gnuastro/manual/html_node/Invoking-asttable.html),
   this will be much more easy (still on the command-line).
    ```shell
    # Show the line that contains `SPECZ'
    grep --color SPECZ header.txt

    # With `-i' grep with "Ignore" the case, so it doesn't have to be all-caps.
    grep --color -i specz header.txt

    # Now search for columns related to the F435w filter.
    grep --color -i f435w header.txt

    # You can also search for two strings (here the f606w and f775w filters):
    grep --color -i 'f606w\|f775w' header.txt
    ```

1. Use [AWK](https://en.wikipedia.org/wiki/AWK) to select and play
   with columns and their values.  We'll save into another sub-data
   file with the ID, spectroscopic redshift, three magnitudes and a (a
   new column which is the calculated color).
    ```shell
    # Select the necessary columns and also add a new one (subtracting two
    # magnitudes to estimate the "color").
    #   - In AWK, columns around identified with a `$' behind their number.
    #   - The `!/^#/' at the start says to ignore lines starting with a `#',
    #     similar to the grep command above.
    awk '!/^#/{print $1, $94, $10, $11, $12, $10-$12}' catalog.txt > magnitudes.txt
    cat magnitudes.txt

    # Select objects with color redder than 3.
    awk '$6>3' magnitudes.txt > reddest.txt
    cat reddest.txt

    # Only select those with a spectroscopic redshift.
    awk '$6>3 && $2!="nan"' magnitudes.txt > reddest-with-z.txt
    cat reddest-with-z.txt
    ```

1. Too many lines? Use [WC](https://en.wikipedia.org/wiki/Wc_(Unix))
   (short for word-count) to count the number of lines, words and
   characters.
    ```shell
    # Print the number of lines, words and characters.
    wc reddest-with-z.txt

    # Each component can also be requested individually.
    wc -l reddest-with-z.txt
    wc -w reddest-with-z.txt
    wc -c reddest-with-z.txt

    # Let's see how many rows the initial catalog and the reddest catalogs had.
    wc -l magnitudes.txt
    wc -l reddest.txt
    ```

1. Redirection **to read** from file
    ```shell
    wc -l < reddest-with-z.txt
    ```

1. Pipes. You can use the result of one command to feed the output
   into another command, this is the spirit of Unix-like operating
   systems. But *some* software don't fully support it.
    ```shell
    # To change a string with SED:
    echo "hello IAC" | sed 's/h/H/g'

    # To read the contents of a file, then feed that into WC to count the
    # number of lines.
    cat reddest-with-z.txt | wc -l

    # To find all files under current directory (and possible
    # sub-directories) that have a certain string, we will discuss
    # `find' later.
    find . | grep reddest

    # To find the process ID and information of a running program,
    # For example if you are running Firefox:
    ps -e | grep firefox
    ```

1. Another useful pipe example. Count the number of files in the
   current directory by listing them with `ls` and pipe it to `wc`.

    ```shell
    ls
    ls *.fits | wc
    ls *.txt  | wc
    ls | grep reddest
    ls -l | grep --color reddest
    ```

1. Use [sort](https://en.wikipedia.org/wiki/Sort_(Unix)) to sort the
   columns, we'll sort by second column (which was spectroscopic
   redshift).

    ```shell
    # Sort second column AS CHARACTERS
    sort -k2 reddest-with-z.txt

    # Sort second column AS NUMBERS
    sort -k2 -n reddest-with-z.txt

    # Select the lowest-highest redshifts.
    sort -k2 -n reddest-with-z.txt | head -1
    sort -k2 -n reddest-with-z.txt | tail -1

    # Only print the redshift
    sort -k2 -n reddest-with-z.txt | tail -1 | awk '{print $2}'

    # With more reasonable precision
    sort -k2 -n reddest-with-z.txt | tail -1 | awk '{printf "%.3f\n", $2}'
    ```

1. Explain variables
    ```shell
    # Variables are defined with `=' after their name,
    # and expanded with a `$' before their name.
    echo 2
    a=2
    echo $a

    # They can be used for any string-of-characters, like a file name:
    table=reddest-with-z.txt
    echo $table

    # For example these two commands are equivalent.
    cat reddest-with-z.txt
    cat $table
    ```

1. Basic arithmetic on variables with integer values (in GNU Bash):
    ```shell
    # Define `a' and `b'
    a=1
    b=2

    # Print them in one line (just to check)
    echo "$a $b"

    # Define `c' to be the sum of `a' and `b' and check its value.
    c=$((a+b))
    echo $c
    ```

1. Generate random number (only in GNU Bash); this is a value between
   0 and 32767 (or 2^15-1).
    ```shell
    # Each time you'll see a different value returned.
    echo $RANDOM
    echo $RANDOM

    # If you want a value between 0 and 100 (or any other maximum),
    # you can just calculate its modulo (or remainder):
    echo $((RANDOM%100))
    echo $((RANDOM%100))

    # If you want it for a file-name, prefix it with the name:
    echo galaxy-RANDOM

    # You can use AWK for floating point random values, for example
    # one number or 10. However, to be reproducible, AWK's random
    # number generator seed is fixed. So the sequence is always the
    # same.
    echo | awk '{print rand()}'
    echo | awk '{for(i=1;i<=10;i++) print rand()}'

    # If you want a different result everytime, you can use the
    # `$RANDOM` variable as a seed.
    echo | awk 'BEGIN{srand('$RANDOM')}{for(i=1;i<=10;i++) print rand()}'

    # You can simultaneously use this to add random columns to your
    # catalogs for testing your analysis for example.
    awk 'BEGIN{srand('$RANDOM')}{print $0, rand()}' reddest-with-z.txt

    # You can also put this column in the middle of others:
    awk 'BEGIN{srand('$RANDOM')}{print $1, rand(), $2}' reddest-with-z.txt

    # You can also have a better text formatting with AWK (using `printf'
    # instead of `print'):
    awk 'BEGIN{srand('$RANDOM')}{printf "%-8d%-8.3f%-.3f\n", $1, rand(), $2}' reddest-with-z.txt
    ```

1. Conditional `if...else` statements.
    ```shell
    # Generate a random number and save its value in `x', then
    # see if it is in the top or bottom half of the full range.
    x=$RANDOM
    echo $x
    if [ $x -gt 16384 ]; then echo "$x top"; else echo "$x bottom"; fi

    # With a semi-colon between them, you can call any set of commands
    # after each other in one line.
    x=$RANDOM; if [ $x -gt 16384 ]; then echo "$x top"; else echo "$x bottom"; fi
    ```

1. Use `while` to parse a file (sorted-by-z.txt) line by line.  Name
   each object (starting from 1) and associate each one to its own
   file to collect results from future analysis (e.g., calculated
   stellar mass).
    ```shell
    # Parse line-by-line and use values to print stentance.
    # Each line is broken into three tokens:
    #    id: first column/token
    #    z:  second column/token
    #    magnitudes: rest of the line (not used here!).
    cat reddest-with-z.txt | while read -r id z magnitudes; do echo "Object $id is at redshift $z"; done

    # Parse line-by-line, and do a shell operation (make a file for each line).
    # We define a `counter' variable (initialized to 1) that is incremented
    # after every line is read and save the files in a `sample' directory.
    mkdir sample
    counter=1
    cat magnitudes.txt | while read -r id z rest_of_line; do echo "$id $z" > sample/$counter.txt; ((counter+=1)); done
    ls sample/

    # We can also do this with AWK like below. But `while` is much more
    # powerful: you can run any command(s) or do any processing!
    rm sample/*.txt
    awk '{print $1, $2 > "sample/"c++".txt"}' magnitudes.txt
    ls sample/
    ```

1. Generating a sequence of numbers using `seq`.
    ```shell
    # Print sequence of numbers to 10 (by default: starting from 1, steps of 1).
    seq 10

    # Print sequence of numbers from 5 to 10 (by default steps of 1).
    seq 5 10

    # Print sequence of numbers from 50 to 10 with a step of 0.5
    seq 5 0.5 10
    ```

1. Use `seq` in a `for` loop.
    ```shell
    # Targets can be selected in a numeric order with `seq', here only
    # printing the first 5.
    for i in $(seq 5); do echo "Galaxy $i"; cat sample/$i.txt; echo; done

    # Check the existence of a file
    for i in $(seq 1 20); do if [ -f sample/$i.txt ]; then echo "$i.txt exists"; else echo "$i.txt doesn't exists"; fi; done

    # If order doesn't matter, `ls' can be used, but it sorts files based
    # on characters, not numerically (10 goes before 2).
    for i in $(ls sample/*.txt); do echo "file $i"; cat $i; echo; done
    ```

1. Use `gzip` and `tar` for compress and decompress files. Gzip is
   the most popular compression standard, but these alternatives also
   exist: 1) `zip` and `unzip` and 2) `bzip2` and `bunzip2`.
   ```shell
   # Gzip can be used to compress files directly (Gzip compresses only one file!)
   ls -lh catalog-raw.txt
   gzip catalog-raw.txt
   ls -lh catalog-raw.txt.gz

   # To uncompress a Gzipped file, use `gunzip':
   gunzip catalog-raw.txt.gz

   # With `tar', you can package any number of files, and it can also
   # automatically compress them in one file.
   # Options: -c create package
   #          -v verbose
   #          -f filename of output (next argument).
   tar -cvf catalog.tar catalog.fits
   tar -cvf outputs.tar *.txt

   # Let's see the size of the packaged text files.
   ls -lh outputs.tar

   # You'll notice this is the same size as the text files that went into
   # the package (combined):
   ls -lh *.txt

   # We can now compress them with gzip
   gzip outputs.tar

   # But if we add the `z' option, `tar' will also run gzip for us :-).
   tar -czvf outputs.tar.gz *.txt

   # You can also use `tar' to uncompress and unpack the tarballs,
   # like below where we build a directory and unpack the tarball
   # above there.
   mkdir unpack
   cd unpack
   tar -xzvf ../catalog.tar.gz
   ```

1. Use `history` to show previous commands.
   ```shell
   # Print last 500 commands
   history

   # Print the last 10 commands
   history 10

   # If you want previous invocations of a command:
   history | grep wget

   # To remove the numbers (first column) we can use AWK:
   history 10 | awk '{$1=""; print}'

   # Now we can save this into a file (or a shell-script, ending in `.sh`).
   history 10 | awk '{$1=""; print}' > test-script.sh
   ```

1. Searching previous commands with Ctrl+r (or `^r`).

1. Almost all command-line programs have "Man pages" (or manual pages)
   which you can access with the `man` command.  To search a man page
   for a certain word, press `/` and write your search item. To go to
   the next occurance, press `n`. Man pages are good, but their
   problem is that they are usually very technical and are only in one
   (long!) page.

   ```shell
   man grep             # press `q' to quit.
   man sed
   man awk
   ```

1. GNU Info is the more modern format of documentation, allowing
   access to a full book in many cases directly on the command-line.
   All GNU programs have it, as well as many others:
   ```shell
   info ls              # Press `q' to quit.
   info grep
   info mkdir
   info awk
   info info            # Best way to learn `Info'! Read this!
   ```

   For example the GNU AWK info page above is equivalen to the
   "Invoking AWK" sections of the
   [web](https://www.gnu.org/software/gawk/manual/gawk.html) or its
   [PDF](https://www.gnu.org/software/gawk/manual/gawk.pdf) manuals.
   Here are some tips to navigate in Info pages:

   1. You can search by pressing `s`.

   1. Almost all pages have "Menus" pointing to their subsections at
      their bottoms.

   1. Any under-lined text is a link to another part of the book. You
      can simply click on it and you'll be taken to that part of the
      book. You can simply press `l` to return to the point you
      left-off.

   1. By pressing `u` you can come to the higher-level section/chapter.

   1. For example to Get to GNU AWK's Numeric functions This is what
      to do, every key to press is shown in square brackets.
   ```shell
   info awk               # Takes you to the "Invoking AWK" Section
   --> [u]                # Takes you to the top AWK book.
   --> [m] [Functions]    # Goes into the "Functions" chapter.
   --> [TAB]              # Press one TAB key to go to the menu
   --> [Enter]            # Press Enter on the first menu item ("Built-in").
   --> [TAB] [TAB]        # To get to the "Numeric Functions".
   --> [SPACE]            # To go down page-by-page.
   ```





### If we have time
1. Discuss `find`.

1. Discuss `sleep` (to wait in the loop).

1. Show the power of while (and the infinite loop).

1. Use `ln` for making symbolic link. It allows to keep free space.
   Let's create a symbolic link for the original catalogue.
   (In Microsoft Windows this is called "shortcut").
    ```shell
    # Make a symbolic link of the catalog
    ln -s catalog.fits link.fits

    # Visually see that `link.fits' is just pointing to `catalog.fits', without
    ls -l *.fits
    ```
