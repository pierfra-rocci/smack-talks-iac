SMACK 16: LaTeX to build professional documents (session 1)
-----------------------------------------------------------

### Abstract

LaTeX is a professional typesetting system to create a ready to print/publish (usually PDF!) document (usually papers!).
LaTeX is the format used by arXiv, and many journals, when you want to submit your scientific papers.
With LaTeX, you "program" the final document: text, figures, tables, bibliography and etc, through a plain-text (source) file.
When you run LaTeX on your source, it will parse your LaTeX source and find the best way to blend everything in a nice and professionally design PDF document.
Therefore LaTeX allows you to focus on the actual content of your text, tables, plots, and not have to worry about the final design (the "style" file provided by the journal will do all of this for you automatically).
This is in contrast to "What-You-See-Is-What-You-Get" (or WYSIWYG) editors, like Microsoft Word or LibreOffice Writer, which force you to worry about style in the middle of content writing (which is very frustrating).
Since the source of a LaTeX document is plain-text, you can use version-control systems like Git to keep track of your changes and updates (Git was introduced in SMACK 5, SMACK 6 and SMACK 8).
This feature of LaTeX allows easy collaboration with your co-authors, and is the basis of systems like Overleaf.
Previously (in SMACK 11), some advanced tips and tricks were given on the usage of LaTeX.
This SMACK session is aimed to complement that with a hands-on introduction for those who are just starting to use LaTeX.

- **Recorded video:** https://www.youtube.com/watch?v=9hDSAJw8ZTY
- **Session 2:** See the [lecture notes for SMACK 17](smack-17-latex-b.md).





### Context and History
- Previously in SMACK 11 some advanced LaTeX tips and tricks were presented.
In this SMACK, we will focus on the very basics of LaTeX, for those who are just getting started.
- TeX was **created by [Donald Knuth](https://en.wikipedia.org/wiki/Donald_Knuth)** (currently professor emeritus at Stanford University), for writing his seminar book [The Art of Computer Programming](https://en.wikipedia.org/wiki/The_Art_of_Computer_Programming) (which is unofficially, the bible of low-level algorithm design in computer science to this day).
    - The _first edition_ of this book was published in _1965_.
    - _Second edition_ **ready in 1976**, but needed _new typesetting_ and the publisher had problems with doing that!
- Knuth _postponed the publication_ and started working on a type-setting system that won't have the problem of updating!
    - **METAFONT** was created for a mathematical definition of building fonts (see [Wikipedia](https://en.wikipedia.org/wiki/Metafont)).
    - **Computer Modern** font was created with METAFONT for scientific writing (see [Wikipedia](https://en.wikipedia.org/wiki/Computer_Modern)).
    - **TeX** was created for typesetting and released in 1978 (see [Wikipedia](https://en.wikipedia.org/wiki/TeX)).
- TeX was released as **Free Software**.
- The _second edition_ finally started **in 1981**, and completed in 1998.
- TeX itself is very low-level, so in 1984, [Leslie Lamport](https://en.wikipedia.org/wiki/Leslie_Lamport) released a set of wrappers over core TeX commands to be easier for every-day usage.
    - The macros became very popular, and are known as LaTeX!
    - You can read **LaTeX** as "**Lamport's TeX**"!
    - LaTeX's **engine is TeX**, LaTeX is just wrappers over TeX for _easier human-writing_.
- **Fun fact**: the versions of TeX are based on the number `$\pi$`.
  For every version, a new digit is added!
  For example, the current version is `3.141592653`, the next version will be `3.1415926535` (a new `5` has been added!).





### Your first document

In case you don't already have LaTeX installed, see the last section below (which descibes its installation: easy, robust and fast).

Starting to write a LaTeX document is very easy! Write the following lines in a plain-text file called `report.tex`. This is the main "source" of your LaTeX document.

```latex
\documentclass{article}
\begin{document}
This is the first line of my report in \LaTeX{}.
\end{document}
```

Here is a detailed break-down of the lines above (and what they mean: if its not already clear!):
* `\documentclass{article}`: this tells LaTeX that you want to use the generic "article" _style_. Later, when you want to use the style-file of a journal for example, you should change this part.
* `\begin{document}`: as the command suggests, this line tells LaTeX that your document actually starts here. As you will see below, the settings (or "preamble") of LaTeX should be placed before `\begin{document}`.
* `\end{document}`: Anything between this line and the `\begin{document}` above are the actual things that are printed in the final output. As the name suggests, this line signifies that the document has ended! LaTeX will not parse anything after this!

To "compile" your LaTeX source into a PDF, you should use the `pdflatex` command on the command-line:

```shell
$ pdflatex report.tex
```

You should now have a `report.pdf` in the same directory. Open it, and have a look:

```shell
$ ls
report.aux  report.log  report.pdf  report.tex
```

Besides your actual source (`report.tex`) and the PDF output (`report.pdf`), you see two extra files that are automatically created.
* `report.aux`: is an "auxilary" file that contains some low-level information when LaTeX should be run multiple times. For example, to generate links to various sections within the text, after a first run, LaTeX will find where everything is, and put that information in this auxilary file. In a second run, LaTeX will use the information in these auxilary files to construct those links.
* `report.log`: is the "log" file, which records all the outputs that were printed on the command-line after you ran `pdflatex`  (and more!). This is very useful if you confront errors and your terminal output was lost (for example you closed the terminal).





### Lines, paragraphs and spaces

As you see above, what you write in your source file is processed before the creation of the final PDF file.
During the processing, LaTeX has some conventions about spaces and lines, some of the most common ones are listed below.
Please run each one of these manually (by calling `pdflatex`) and confirming this.
It is important to understand this distinction between you plain-text source and the built PDF files.
* Any number of SPACE characters between words in our source will be shown as a single space in the PDF.
Therefore, if you modify your source to become like below, it will have *no effect* on your output PDF.
This is another advantage of having a processing phase between
     ```latex
     \documentclass{article}
     \begin{document}
     This is  the    first    line of       my report in \LaTeX{}.
     \end{document}
     ```
* A single new-line is ignored! In other words, the output PDF of the following LaTeX source will also be identical to the above two cases.
     ```latex
     \documentclass{article}
     \begin{document}
     This is  the    first    line of
     my report in \LaTeX{}.
     \end{document}
     ```
* To separate one paragraph from another, in LaTeX you need to put an empty line between them:
     ```latex
     \documentclass{article}
     \begin{document}
     This is the first line of my report in \LaTeX{}.

     This is my second paragraph.
     \end{document}
     ```



### Title, author(s) and date

A document isn't just the main body of text!
We also need a title, author list, creation date and etc.
Fortunately adding these into the paper is very easy!
Simply give a value to the `\title` and `author` variables _in the preamble_ (the space between `\documentclass{...}` and `\begin{document}`.
Afterwards, within the `\begin{document}` and `\end{document}`, insert a `\maketitle`.

```latex
\documentclass{article}

\title{My exciting project}
\author{My Name, My colleage}

\begin{document}
\maketitle
This is my first document.
\end{document}
```

Similar to before, to compile your source into a PDF, simply run

```shell
$ pdflatex report.tex
```

You now see your title in slightly larger font in the horizontal center of the page, the authors are placed under it (again in the center), and you also see the date that is automatically printed!
Try removing the `\maketitle` line (but keep the `\title` and `\author`).
You will see that no title, author or date is printed (even though you still have `\title` and `\author`).
As we explained before, only things within `\begin{document}` and `\end{document}` are actually printed in your PDF, the things in the preamble, are settings about what to print and how to print it.

As its name suggests, the job of `\maketitle` is to make the title based on the "style" of the document (that is given to `\documentclass`).
Therefore later, when you use the style of a certain journal, your title, and authors will automatically fall exactly into the format/style of that journal, and you don't have to touch any other part of your LaTeX source.
You can now see the "programming" aspect of LaTeX!





### Comments within your LaTeX source

Since your LaTeX source file isn't "What-You-See-Is-What-You-Get", and is more like the plain-text source of your programming (for example in Python or Shell scripts), you also have nice features like comments (to tell LaTeX to ignore anything after a special character).
In Python or Shell scripts, this character is `#`, but in LaTeX it is `%`!
Like your programming, using comments is always good and encouraged, because it makes your source more easily readable (especially in the pre-amble).
Have a look at the source below and compare it with the above!
With those direct comments to a human (not to LaTeX), doesn't it just feel more friendly?

```latex
\documentclass{article}

% First-page information.
\title{My exciting project}
\author{My Name, My colleage}

% Start of printable content.
\begin{document}

% Build the title, authors and date.
\maketitle

This is my first document.
\end{document}
```





### Longer text and the corresponding issues

You reports will usually be longer one line!
Let's fill the page with some pre-written text and see how the default `article` sets all those words in to the PDF!
In the example below, we have taken the first three paragraphs of the [Wikipedia page on Stars](https://en.wikipedia.org/wiki/Star).

```latex
\documentclass{article}

% First-page information.
\title{My exciting project}
\author{My Name, My colleage}

% Start of printable content.
\begin{document}
\maketitle

A star is an astronomical object comprising a luminous spheroid of plasma held together by its gravity.
The nearest star to Earth is the Sun.
Many other stars are visible to the naked eye at night, but their immense distances from Earth make them appear as fixed points of light.

A star's life begins with the gravitational collapse of a gaseous nebula of material composed primarily of hydrogen, along with helium and trace amounts of heavier elements.
Its total mass is the main factor determining its evolution and eventual fate.

Stellar nucleosynthesis in stars or their remnants creates almost all naturally occurring chemical elements heavier than lithium.
Stellar mass loss or supernova explosions return chemically enriched material to the interstellar medium.
They are then recycled into new stars.
\end{document}
```

Here is one important point that you may have already noticed: the fact that there is only **one sentence per line**!
Since LaTeX ignores a single new-line between text, one very useful convention is to have one sentence per line.
Here is why this is useful:
- It will clearly remind you _when your sentence has become too long_ (and thus hard to read!).
In a professionally written text, the sentences are short, giving the reader some time to digest one concept in one sentence (instead of blending many concenps with "which" and "that", just start a new sentence/line!).
- It helps later when you want to _track the changes_ in your document with version-control tools like Git.

Try compiling the source above and have a look at the resulting PDF:

```latex
pdflatex report.tex
```

Do you see how much empty space there is around the text?
These kinds of things are defined at the very start of your source (in the `article` class).
For example, try changing the class to `book`:

```latex
\documentclass{book}
```

You will see that the title, authors, and date (that were determined in the preamble) are now alone in the first page (like a book!), and the actual text starts on page 2!
You will also notice that like a book, the text is no longer in the center of the page, but the left margin is larger than the right margin!
If you add more contents (copy-paste from Wikipedia), you will see that in the third page, the right margin is larger (which is due to the book binding).





### Equations

If you have a new look at the [Wikipedia page on Stars](https://en.wikipedia.org/wiki/Star) again, you will notice that close to the end of the first paragraph, there were two mathematical expressions ($`10^{22}`$ and $`10^{24}`$).
But if you look at your PDF, you will see that they are printed simply as 1022 and 1024!
To insert **in-line equations** in LaTeX, you need to enclose the equation within two `$`.
For example, that particular sentence from Wikipedia should be written like this:

```latex
The observable universe contains an estimated $10^{22}$ to $10^{24}$ stars.
```

Make this conversion in your own `report.tex`, and you will see that the powers of 10 have been properly type-set.
In mathematical mode (within two `$` signs), the `^` takes its argument up (like the "power" operator).
By default, the first character after `^` is assumed to be its argument, so if you simply use `10^2`, you will see $`10^2`$.
However `10^22` will display as $`10^22`$ (recall that `^` only applies to the first character after it).
To take any number of character to the power, you need to put them in `{}`, or `10^{22}`.
The `_` operator is the opposite, and takes its argument to below the previous character.
Therefore, if you want to have $`M_{ij}^2`$ in your PDF, you should write `M_{ij}^2`.

Another form of equations in a scientific document, are those that take one whole line, and are placed in its center.
For those equations, you should enclose the equation source between two `$$`s.
For example the source below (that you can copy in `report.tex` to compile and see the result).

```latex
If an object of magnitude $m_r$ has brightness $B_r$, then an object of brightness $B$ will have a magnitude of $m$ that is calculated through this equation:

$$m-m_r=-2.5\times log_{10}\left(\frac{B}{B_r}\right)$$
```

The equation above will be shown like this:

```math
m-m_r=-2.5\times log_{10}\left(\frac{B}{B_r}\right)
```

If you want the equation to be identifiable (given a counter) in the final PDF, you have to use the `equation` environment:

```latex
\begin{equation}
  m-m_r=-2.5\times log_{10}\left(\frac{B}{B_r}\right)
\end{equation}
```

Generally, LaTeX has a very rich tool set to professionally write complex equations (this is historically one of the reasons TeX was created: TeX is the core engine under LaTeX).
One of the best resources to get more familiar with it is the [respective section of the Wikibook on LaTeX](https://en.m.wikibooks.org/wiki/LaTeX/Mathematics).





### Dividing text within sections and sub-sections

Dividing the text into sections and sub-sections, makes it much more easier to read.
In LaTeX, you simply put a `\section{TITLE}` above the first line of the paragraph where you want the section to start.
Just replace `TITLE` with your desired title for the section.
By default in the `article` class, LaTeX will add a counter before your given name and print it in larger font.
It will also not indent the first paragraph as is common in many publishing scenarios.
You can also have sub-sections and sub-sub-sections within each section!
Simply use `\subsection{TITLE}` and `\subsubsection{TITLE}` in a similar manner.

```latex
\documentclass{article}

% First-page information.
\title{My exciting project}
\author{My Name, My colleage}

% Start of printable content.
\begin{document}
\maketitle

\section{Introduction}
A star is an astronomical object comprising a luminous spheroid of plasma held together by its gravity.
The nearest star to Earth is the Sun.
Many other stars are visible to the naked eye at night, but their immense distances from Earth make them appear as fixed points of light.

\section{A star is born}
A star's life begins with the gravitational collapse of a gaseous nebula of material composed primarily of hydrogen, along with helium and trace amounts of heavier elements.
Its total mass is the main factor determining its evolution and eventual fate.

\subsection{Chemical aspects}
Stellar nucleosynthesis in stars or their remnants creates almost all naturally occurring chemical elements heavier than lithium.
Stellar mass loss or supernova explosions return chemically enriched material to the interstellar medium.
They are then recycled into new stars.
\end{document}
```





### Loading packages (`geometry` for setting the print size)

By default LaTeX is very minimal!
It only has basic features to create the most basic document.
To add functionality, LaTeX has "packages" that you can use (or load) with the `\usepackage{}` command.
As a demonstration, here we'll use the [`geometry` package](https://www.ctan.org/pkg/geometry) to make the width and height of the printed document cover more of the space within the page.
To avoid long examples, we'll use the basic two-line-within-the-body example from before, but you can do this on a longer document to be more clear:

```latex
\documentclass{article}

% Set the size of the page and total width and height of text
\usepackage{geometry}

% First-page information.
\title{My exciting project}
\author{My Name, My colleage}

% Start of printable content.
\begin{document}

% Build the title, authors and date.
\maketitle

This is my first document.

And here is the second line.
\end{document}
```

After compiling this LaTeX source into a PDF, you will notice that the text has become wider.
The `geometry` package is very powerful for setting the widths, of various components in the page, see [its manual](http://mirrors.ctan.org/macros/latex/contrib/geometry/geometry.pdf) for more details.

To **customize a LaTeX command**, you should put them in `[]`s before the command.
For example, to pass the `a4paper` option to Geometry, we should use `\usepackage[a4paper]{geometry}`.
Below, we'll add the following two options that you may regularly need when writing a document from scratch (not using any style-file).
Try changing the values to see the effect.
* `a4paper`: set the general page size to be A4.
* `total={17cm, 26cm}`: the width of the text body will be 17cm, and its height will be 26cm.

```latex
% Set the size of the page and total width and height of text
\usepackage[a4paper, total={17cm,26cm}]{geometry}
```





### Tables

Tables are one of the common features in a scientific report.
Writing tables is very easy with LaTeX.
Within the main body of your paper (between `\begin{document}` and `\end{document}`), make a `tablular` environment like below.
Here are some notes on how to interpret this:
* The `&`s separate the columns within one row.
* The `\\` go to the next line.
* After the `\begin{tabular}`, you need to specify the positioning of the contents within each cell.
This is the purpose of the `{c c c}` after the `\begin{tabular}`: we say that we want the contents of all three columns to be "c"enter-aligned.
Instead of a `c` for center, you can also use `l` for left, or `r` for right.

```latex
\begin{tabular}{c c c}
    A & B & C \\
    D & E & F \\
    G & H & I \\
\end{tabular}
```

You can put vertical lines between any of the columns, by simply putting a `|` between the characters of the formatting string like below (where no vertical line is placed between the first and second columns intentionally: to show that it is optional).

```latex
\begin{tabular}{| c c | c}
```

To put vertical lines in your table, you can use `\hline` (short for _horizontal line_):

```latex
\begin{tabular}{c c c}
  \hline
  A & B & C \\
  \hline
  D & E & F \\
  G & H & I \\
  \hline
\end{tabular}
```

You can make very complex tables with LaTeX, to learn more, you can see the respective [Wikibooks](https://en.m.wikibooks.org/wiki/LaTeX/Tables) or [Overleaf](https://www.overleaf.com/learn/latex/Tables) pages.





### Images

Inserting images in LaTeX is also very easy.
For the example here, let's use [one of the images](https://upload.wikimedia.org/wikipedia/commons/7/71/Star_types.svg) from the [Wikipedia page on stars](https://en.wikipedia.org/wiki/Star) that we also used before.
To help in file-organization, it is better that you put all your images in a special sub-directory of the

```shell
# Directory only for images.
mkdir img

# Go into the 'img' directory:
cd img

# Download the image
wget https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Star_life_cycles_red_dwarf_en.svg/640px-Star_life_cycles_red_dwarf_en.svg.png

# Give it a more friendly name
mv 640px-Star_life_cycles_red_dwarf_en.svg.png star-life.png
```

To insert an image into your LaTeX document, you need to load (or "use", in LaTeX terminology) the [`graphicx` package](https://ctan.org/pkg/graphicx).
So add this line in your preamble:

```latex
% To include graphics within the document.
\usepackage{graphicx}
```

Afterwards, you can use the `\includegraphics` command within the actual printed body of your document (within `\begin{document}` and `\end{document}`) to insert an image.
You can use `\includegraphcs` as many times as you want for as many images as you want.
It is just important to remember that when you are using `pdflatex`, you can use images with the following formats: `.pdf`, `.jpg`, `.png`.
See the complete example below:

```latex
\documentclass{article}

% To include graphics within the document.
\usepackage{graphicx}

% Set the size of the page and total width and height of text
\usepackage[a4paper, total={17cm,26cm}]{geometry}

% First-page information.
\title{My exciting project}
\author{My Name, My colleage}

% Start of printable content.
\begin{document}
\maketitle

\section{Introduction}
A star is an astronomical object comprising a luminous spheroid of plasma held together by its gravity.
The nearest star to Earth is the Sun.
Many other stars are visible to the naked eye at night, but their immense distances from Earth make them appear as fixed points of light.

\section{A star is born}
A star's life begins with the gravitational collapse of a gaseous nebula of material composed primarily of hydrogen, along with helium and trace amounts of heavier elements.
Its total mass is the main factor determining its evolution and eventual fate.

% Life-cycle of star, from Wikipedia
% Image from: https://en.wikipedia.org/wiki/Star#/media/File:Star_life_cycles_red_dwarf_en.svg
\includegraphics{img/star-life.png}

\subsection{Chemical aspects}
Stellar nucleosynthesis in stars or their remnants creates almost all naturally occurring chemical elements heavier than lithium.
Stellar mass loss or supernova explosions return chemically enriched material to the interstellar medium.
They are then recycled into new stars.

\end{document}
```

Bellow we'll review some basic LaTeX tools that will help in properly displaying this raw image.
All the points below can be applied to any LaTeX construct, not just images.
* **Indentation**: The image is slightly indented compared to the text.
This is happening because all new paragraphs (except the first one!) are indented and the image is within a paragraph.
To disable indentation for a paragraph (in general; not just for images!), you can use the `\noindent` command in the line just ontop of the image:
    ```latex
    \noindnet
    \includegraphics{img/star-life.png}
    ```
* **Width of image**: The image is larger than the page width and its righer-side has gone outside of it!
You can fix this by customizing the width of the image with `width` option.
Recall that you can customize LaTeX commands by placing parameter names and values before command name in square brackets:
    ```latex
    \noindent
    \includegraphics[width=5cm]{img/star-life.png}
    ```
* **Relative widths**: Please compile the PDF document after the previous change.
You will see that the image is now too small!
The problem is that you have given the width in an absolute measure (`5cm`).
So LaTeX will make sure it is always 5cm, independent of the size of your page.
This can be annoying, because you'll have to change these widths manually for different page sizes!
It is therefore better to set the width in a relative measure!
For such purposes, we have the `\linewidth` macro that will always expand to the width available for writing.
You can simply replace `5cm` with `\linewidth`.
After compiling the source above with the following change, you will see that the image fits nicely within the usable line width.
    ```latex
    \noindent
    \includegraphics[width=\linewidth]{img/star-life.png}
    ```
* **Fraction of relative width**: By putting a fraction (between 0 and 1) after the `=` and before `\linewidth`, you can tell LaTeX to use only a fraction of the width.
For example, below, we'll set the image to only cover 80% of the line's width:
    ```latex
    \noindent
    \includegraphics[width=0.8\linewidth]{img/star-life.png}
    ```
* **Centering**: To put your image in the "center" of the width, you can place it with a the `center` environment.
Note that in this scenario, you don't need the `\noindent` any more because the `center` environment is independent of the text paragraphs.
    ```latex
    \begin{center}
      \includegraphics[width=0.8\linewidth]{img/star-life.png}
    \end{center}
    ```
* **Cropping**: if you only want to show part of the image, you can "clip" the image by adding the following two options: `trim=A B C D, clip`. `A` is the distance to clip from the left-side of the image, `B` is the distance to clip from the bottom of the image, `C` and `D` are the distances to clip from the right and top of the image respectively.
For example with the command below, we'll clip 5cm of the right side of the image before displaying it:
    ```latex
    \includegraphics[trim=5cm 0cm 0cm 0cm, clip, width=0.8\linewidth]{img/star-life.png}
    ```
* **Rotating the image**: You can rotate the included graphics with the `angle` option (which takes the anti-clockwise rotation angle in degrees):
    ```latex
    \begin{center}
      \includegraphics[angle=10, width=0.8\linewidth]{img/star-life.png}
    \end{center}
    ```





### Floating components in a page; adding captions

In the two sections above, you inserted tables and graphics into your document.
However, in the basic usage above, their position is fixed and locked to the same position within the source (as part of the text).
You alreadys saw an example of this in the example above on inserting images: that the image had the same indentation as a normal text!
When you simply use the raw `\includegraphics`, the image will always be locked between the precise components.
This is good and required in some scenarios, but usually it will be too restrictive: for example, if you paragraph goes close to the bottom of the page, most of the image will not be seen at all (only its top: the rest will be outside the page boundaries).

LaTeX has the `figure` or `table` environments precisely for this purpose!
When you define something within a `figure` or `table`, it won't be locked to the same place within the source of your document.
You are telling LaTeX that it is free to move those things to other places for best demonstration, or that it can "float" them.
For example, let's wrap the `center` environment we defined above for including the image, into a `figure`:
```latex
\begin{figure}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{img/star-life.png}
  \end{center}
\end{figure}
```

After compiling the source into a PDF, you will see that the image is no longer with those particular paragraphs:
it has "float"ed to the next page!
If you **add text** from Wikipedia for two more pages, you will notice that it has been moved to the top of the next page.

If you want the `figure` to be constructed in the **same location** as the source, you should put a `[h]` after it (you can also use `[b]` for "bottom" or `[t]` for top):
```latex
\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.8\linewidth]{img/star-life.png}
  \end{center}
\end{figure}
```

If you want to ensure that the floating environment (`figure` in this case) is placed at the bottom or top of the same output page (as the paragraphs before or after it), you can put a `!` before the `t` or `b`, like this: `[!t]` or `[!b]`.

You can also encapsulate more than one image within a float!
To have the images in one line, just ensure that the sum of their widths isn't more than one times the width of a line:
```latex
\begin{figure}
  \begin{center}
    \includegraphics[width=0.45\linewidth]{img/star-life.png}
    \includegraphics[width=0.45\linewidth, angle=180]{img/star-life.png}
  \end{center}
\end{figure}
```

The **`table`** floating environment is very similar to `figure` above, but for tables (when you use the `tabular` environment; as described above).

Floats have another important and useful feature: **captions**: To add captions to a float, simply define a `\caption{}` within the float.
The text of the caption should be placed within the curly braces. It can have any length (but try to keep it short for the sake of the human readers of your document!).
For example see the example below:
```latex
\begin{figure}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{img/star-life.png}
  \end{center}
  \caption{Life cycle of a star.}
\end{figure}
```

As you see above, besides adding a caption, it will also place a "Figure 1:" before your text. In the next figure, this number will increment by one.
So you _don't have to worry about changing the order of figures_: their numbers will be set automatically.

You can also place the `\caption{}` statement before `\includegraphics`, the caption will be _printed on top of the figure_.





### Continue to SMACK 17 for more interesting features

In this session, we reviewed how to make your first LaTeX document from scratch, and the most basic things you should know.
But all of this is not even the tip of the iceburg!
LaTeX has many more features that will greatly help you in writing a professional document, while focusing on your actual content and not on the style!
The lecture notes for SMACK 17 on [more advanced features of LaTeX are available in `smack-17-latex-b.md`](smack-17-latex-b.md).
