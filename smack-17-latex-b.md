SMACK 17: LaTeX to build professional documents (session 2)
-----------------------------------------------------------

### Abstract

In [SMACK 16](smack-16-latex.md), the very basics of LaTeX were introduced; showing how to make a basic document from scratch, set the printable size, inserting images and tables and etc.
In this session, we will go into more advanced features that are also commonly helpful when preparing a professional document (while letting you focus on your exciting scientific discovery, and not have to worry about the style of the output).
These features include automatically referencing different parts of your document using labels (this allows you to easily shuffle figures, sections or tables), making all references to various parts of your text click-able (greatly simplifying things for your readers), using Macros (to avoid repetition or importing your analysis results automatically), adding bibliography, keeping your LaTeX source, and your top directory clean, and finally using Make to easily automate the production of your document.

- **Recorded video:**
- **Session 1:** See the [lecture notes for SMACK 16](smack-16-latex.md).





### Basic introduction to LaTeX (SMACK 16)

If you haven't already seen the [first session on LaTeX (SMACK 16)](smack-16-latex.md), please review those notes (and/or watch [the recorded video](https://www.youtube.com/watch?v=9hDSAJw8ZTY)) before continuing here.
This session starts from where we left-off in SMACK 16.





### Referencing various components

As you saw in the end of the [previous session](smack-16-latex.md), LaTeX can automatically assign numbers to the equations, sections, figures or tables.
This is a wonderful feature to help you focus on writing your exciting science, and not having to worry about counting figures, tables or equations!
However, it will still be hard to reference them within your text manually.
For example, if a figure is initially called "Figure 1", but you later decide to add another figure before it, LaTeX will automatically change that figure's label to "Figure 2".
Therefore, if you had used "Figure 1" anywhere in your text (to reference or cite that figure), you have to manually find and change them!
This is very frustrating and regularly occurs with WYSIWYG software like LibreOffice or Microsoft Word.

Fortunately, LaTeX has a very easy and powerful solution to this: within the environment which you later want to label, place a `\label{}` command and give it a label.
For example, in the block below, we will give a label `eq:magnitude` to the equation environment that was defined above (the `eq:` part is an optional, and human-friendly convention, it is not a LaTeX requirement):

```latex
\begin{equation}
  \label{eq:magnitude}
  m-m_r=-2.5\times log_{10}\left(\frac{B}{B_r}\right)
\end{equation}
```

Later, when you want to reference that equation within your text, you can use `\ref{eq:magnitude}` like below.
As you see, the exact name that was given to `\label{}` should be used.
```latex
For the definition of magnitude, see equation \ref{eq:magnitude}.
```

When you run `pdflatex`, you will notice that in the place of the `\ref{}` there is a "??".
This is happening because you may use `\ref{}` before the actual definition of `\label{}`.
But LaTeX doesn't want to be forced to parse the document two times every time.
So in a first run LaTeX will load all the information that may be necessary in the auxiliary file `report.aux` that we discussed before.
In a **second run of `pdflatex`**, you will see that the "??" is removed and the number of the equation is entered.
With `cat report.aux`, you can have a look all the auxiliary information LaTeX has gathered, and will later use (try finding `eq:magnitude`!).

You can add similar `\label{}` statements within the `\section{}`, `\begin{figure}`, or `\begin{table}` environments mentioned in the previous section.
In the latter two, `\label{}` has to be placed within the `\caption{}` command (that is in charge of giving labels to that particular environment).

See the full example below for using `\label{}` in equations and figures at the same time.
You will also notice that we have used `\pageref{}` which is another useful construct to identify the page containing your desired label automatically.
Just don't forget to run `pdflatex` two times for the labels to be properly placed for the first time.
In later runs of `pdflatex`, if you haven't defined any new `\label`

```latex
\documentclass{article}

% To include graphics within the document.
\usepackage{graphicx}

% Set the size of the page and total width and height of text
\usepackage[a4paper, total={17cm,26cm}]{geometry}

% First-page information.
\title{My exciting project}
\author{My Name, My colleage}

% Start of printable content.
\begin{document}
\maketitle

\section{Introduction}
A star is an astronomical object comprising a luminous spheroid of plasma held together by its gravity.
The nearest star to Earth is the Sun.
Many other stars are visible to the naked eye at night, but their immense distances from Earth make them appear as fixed points of light.
See Section \ref{sec:star-birth}.

\begin{equation}
  \label{eq:magnitude}
  m-m_r=-2.5\times log_{10}\left(\frac{B}{B_r}\right)
\end{equation}

\section{A star is born}
\label{sec:star-birth}
A star's life begins with the gravitational collapse of a gaseous nebula of material composed primarily of hydrogen, along with helium and trace amounts of heavier elements.
See equation \ref{eq:magnitude} for the definition of magnitude
Its total mass is the main factor determining its evolution and eventual fate.
Figure \ref{fig:life-cycle} in page \pageref{fig:life-cycle} shows the life-cycle of a star.

% Life-cycle of star, from Wikipedia
% Image from: https://en.wikipedia.org/wiki/Star#/media/File:Star_life_cycles_red_dwarf_en.svg
\begin{figure}[!b]
  \begin{center}
    \includegraphics[width=0.8\linewidth]{img/star-life.png}
  \end{center}
  \caption{\label{fig:life-cycle} Life cycle of a star.}
\end{figure}

\subsection{Chemical aspects}
Stellar nucleosynthesis in stars or their remnants creates almost all naturally occurring chemical elements heavier than lithium.
Stellar mass loss or supernova explosions return chemically enriched material to the interstellar medium.
They are then recycled into new stars.

\end{document}
```





### Clickable links for the references

Automatically assigning of the counters of the different components is very good!
But that isn't all!
LaTeX can also make them clickable links to help your readers navigate to the part of your text that you are writing about.
To do this, simply use the `hyperref` package.

```latex
% Clickable links to references and citations
\usepackage{hyperref}
```

Put this line in the preamble (before `\begin{document}`) of the fully working example above (that uses `\ref{}`).
But before creating the PDF, let's clean up the auxiliary file with the command below.
We need to do this because the previous run of LaTeX (that didn't use the `hyperref` package) needs to be re-created from scratch to generate the links properly.
This only needs to be done once, you won't have to worry about it any more!

```shell
$ rm report.aux
$ pdflatex report.tex
```

Open the final PDF output and have a look!
You will see that a red box has appeared around all your references.
If you click on those red boxes, you will be taken to the respective part that contains its label.
In a long document, your readers will greatly appeciate this feature!

But having red boxes around each link makes the text hard to read.
With the `hyperref` package, you can set the links to have a certain color.
Infact, you can give different colors for different types of links.
To help in readability of your PDF, replace the old `\usepackage{hyperref}` line with the modified version below (where we are setting all the links to blue).
Run `pdflatex` after this change to see the effect:

```latex
% Clickable links to references and citations
\usepackage[colorlinks, urlcolor=blue, citecolor=blue, linkcolor=blue]{hyperref}
```

You will notice that the reference numbers are still clickable, but they aren't covered by a red box any more.
While they are still clickable, they don't jump out with those ugly red boxes any more.
This takes us to a side-tip: when you have many options to a command, you can easily break them up in to a one-line-per-option format like below.
This format won't affect LaTeX, but will greatly help in the human-readability and maintenence of your source:

```latex
% Clickable links to references and citations
\usepackage[colorlinks,
            urlcolor=blue,
	    citecolor=blue,
	    linkcolor=blue]{hyperref}
```

The `hyperref` package has another very useful feature: through the `pdftitle`, `pdfauthor`, `pdfsubject` and `pdfkeywords`, it can set the metadata of the PDF (that you don't directly see in the printed contents, but are visible as "properties" of the PDF).

```latex
% Clickable links to references and citations, and PDF properties
\usepackage[colorlinks,
            urlcolor=blue,
	    citecolor=blue,
	    linkcolor=blue,
            pdftitle={My exciting project},
            pdfauthor={My name, My colleage},
            pdfsubject={My first project with LaTeX},
            pdfkeywords={LaTeX, preparing reports}]
           {hyperref}
```

After compiling your PDF, open its properties (in your PDF viewer) and you will see your title, your name and your given keywords have been inserted in the PDF properties also!





### Macros to avoid repetition

Until this point, we have used the title and authors of the project in two separate places: once in `\title` and `\author` commands, and once as the values to `pdftitle` and `\pdfauthor` for the PDF properties.
In the future, if you change the title, or a co-author is added, and you change one of them, you may forget to change the other and this will cause inconsistencies for your readers.

For such situations, LaTeX has "macros" that are given a value in the preamble and will expand to that value anywhere they are used.
To define a macro, you simply use the `\newcommand` command in the preamble.
So let's define two macros for the project title and authors (put these in the preamble)

```latex
% Custom macros of this project
\newcommand{\projecttitle}{My exciting project}
\newcommand{\projectauthors}{My name, My colleage}
```

You can now use these two macros by simply using `\projecttitle` instead of the title of your project and `\projectauthors` instead of the authors like below.

```latex
% First-page information.
\title{\projecttitle}
\author{\projectauthors}

% Clickable links to references and citations
\usepackage[ colorlinks,
             urlcolor=blue,
	     citecolor=blue,
	     linkcolor=blue,
             pdftitle={\projecttitle},
             pdfauthor={\projectauthors},
             pdfsubject={My first project with LaTeX},
             pdfkeywords={LaTeX, preparing reports}] {hyperref}
```

Now you can be sure that they will expand to an identical string of characters, anywhere they are used!
Again, this will help you focus on describing your exciting scientific project instead of worrying about such mundane issues!

This becomes a much more useful feature within the actual body of your paper, and when refering to your results.
For example, initially you may have found 5 stars that satify your selection criteria, and you may have written this in several places of the text.
Later, you may find an error in you analysis, and find that actually 7 stars should have been selected.
But if you forget to correct all the cases, your readers will get confused.
To solve this problem, you can simply define a macro containing the final number of selected stars in your preamble:

```latex
% Analysis results
\newcommand{\projectnumselected}{5}
```

Then within the body of your paper, you can simply use `\projectnumselected` anywhere you want to refer to the total number of selected stars, like the two fake paragraphs below (try placing them anywhere within the LaTeX source you have).
Note the macro is placed within `$` signs because it is a number, and its more generic to show numbers in equation mode (some journal styles may give them different fonts).

```latex
Following the selection criteria we found $\projectnumselected$ stars.

While we have found $\projectnumselected$ stars, others had found $10$.
```

When defining and using macros there are two important points to consider:
* The **name** of a macro can only contain **alphabetical** characters, so you can't use numbers, dashes, underscores or etc.
* If your macro is a number, place it within `$`s (as with any number). If its not a number, you need to put it within `{}`s, for example `{\macroname}` or `\macroname{}`.
Otherwise the space you put after the macro will not be shown in the PDF (LaTeX will assume the next character its an argument).

LaTeX macros are very powerful, for example and you can also define macros that take arguments and operate based on them.
For more, see the [Macros section of the LaTeX Wikibooks](https://en.m.wikibooks.org/wiki/LaTeX/Macros).





### Importing analysis results
Through macros, you can automatically pass your analysis results to LaTeX directly (without any manual intervention which can be prone to human error!).
The trick is to define a LaTeX macro for each analysis result that you want to report, put them all into one file, and load that file into your LaTeX source.
Let's see how that works.

Assume you want to report how many stars are in the Gaia catalog within a radius of 0.1 degrees of the coordinate 53.15958,-27.78191.
To do this, we'll use [`astquery` of GNU Astronomy Utilities](https://www.gnu.org/software/gnuastro/manual/html_node/Query.html) (Gnuastro, if you don't have Gnuastro installed, see below) with the following simple shell script below that we'll call 'analysis.sh'.
Note the last four `printf` commands where we write the analysis inputs (coordinates and search radius) and output as LaTeX macros in a file called `report.tex`.

```shell
#!/bin/bash

# Parameters
rad=0.1
dataset=dr3
ra=53.15958
dec=-27.78191

# Query the Gaia database.
astquery gaia --dataset=gaia$dataset.gaia_source --center=$ra,$dec \
	 --radius=$rad --output=from-gaia.fits

# Find the number of objects that were downloaded
numq=$(asttable from-gaia.fits | wc -l)

# Convert the dataset name to upper-case (which is common in English
# writing). This is a GNU Bash feature (a single '^' will only make
# the first character upper-case, while '^^' will affect the wole
# word).
datasetup=${dataset^^}

# Write inputs and outputs as LaTeX macros:
printf '\\'newcommand{'\\'projectra}{$ra}"\n"              > results.tex
printf '\\'newcommand{'\\'projectdec}{$dec}"\n"           >> results.tex
printf '\\'newcommand{'\\'projectrad}{$rad}"\n"           >> results.tex
printf '\\'newcommand{'\\'projectnumq}{$numq}"\n"         >> results.tex
printf '\\'newcommand{'\\'projectdataset}{$datasetup}"\n" >> results.tex
```

**In case you don't have the `astquery` command:** The `astquery` program is part of Gnuastro and is just a wrapper for the lower-level `curl` command which is avialable on most computers.
If you don't have Gnuastro (and thus the `astquery` command), simply replace that line in the script above to the following line (which is more complex, but is fine in a script!).
The `astquery` program hides all that complexity in an easy to use command-line program.
Note that this command uses the same variables defined in the shell script (`$ra`, `$dec` and etc), so be careful if you use it in other places.

```shell
curl -ofrom-gaia.fits --form LANG=ADQL --form FORMAT=fits --form REQUEST=doQuery --form QUERY='SELECT  * FROM gaia'$dataset'.gaia_source WHERE   1=CONTAINS( POINT('"'ICRS', ra, dec), CIRCLE('ICRS', '"$ra"', '"$dec"', '"$rad"') )"' ' https://gea.esac.esa.int/tap-server/tap/sync
```

Let's make the script executable, run it, and a look at the contents of `results.tex` with the three commands below.
Recall that we [introduced Gnuastro in SMACK 3](smack-3-gnuastro.md), and [shell scripts in SMACK 4](smack-4-scripting.md).

```shell
$ chmod +x analysis.sh
$ ./analysis.sh
$ cat results.tex
```

You are now ready to load these macros into your main LaTeX source.
To do this, you should use the `\input{FILENAME}` command that takes a filename (`FILENAME`) as its argument.
When confronted with `\input{FILENAME}`, LaTeX will load the contents of the file called `FILENAME` into memory, before continuing to load the rest of the LaTeX source.
`\input` can be used anywhere.
In this scenario, since `results.tex` contains `\newcommand`, it should be placed somewhere in the preamble:

```latex
% Include the analysis results as LaTeX macros
\input{results.tex}
```

You can now use those macros within the main body of your LaTeX document.
For example in this sentence:
Note that for the last macro, we didn't use math-mode (in `$`) because that macro will not expand to a number, but actual English text!
So we are placing it within a `{}` (as explained above).

```latex
Within a radius of $\projectrad$ degrees around the coordinates ($\projectra,\projectdec$), we found $\projectnumq$ stars in Gaia {\projectdataset}.
```

You can now create your PDF to see all those macros replaced with their actual values from the script, without any manual intervention!

```shell
$ pdflatex report.tex
```

If you later change the dataset, coordinates or radius and re-run `analysis.sh`, the values will be automatically updated in the output PDF!
For example, try using the Gaia data release 2 by setting `dataset=dr2` in `analysis.sh`.
Then re-do your analysis and re-generate your PDF: you'll see the total number of stars has changed in the PDF without any manual intervention:

```shell
$ ./analysis.sh
$ pdflatex report.tex
```

It is highly encouraged to directly link your analysis to your paper in a similar manner to avoid human errors and simplify testing.





### Bibliography and citations

A very component of academic documents is bibliography and citation of other works.
In this aspect also, LaTeX is very powerful in automatic generation of the bibliography (like all the cases above, this helps you focus on conveying your actual science, and not have to worry about cosmetics).
The original bibliography management system is called BibTeX, but it is gradually being replaced by the more feature-rich BibLaTeX.

The best way to demonstrate the power of BibLaTeX in building your citations and references is in practice!
Let's cite [Akhlaghi et al. 2021](https://doi.org/10.1109/MCSE.2021.3072860) in our demonstration document (this paper uses many of the concepts demonstrated in the SMACK series, including the linking of analsis outputs to the PDF production for reproducibility).

Most journals provide BibTeX entries for their papers as one way to export citation information.
But the level of detail in each journal is usually different!
In Astronomy, we are lucky to have the [SAO/NASA **Astrophysics Data System (ADS)**](https://ui.adsabs.harvard.edu), which provides a free, fixed format, and very powerful way to search for papers, check citations and etc.

To find a certain paper, usually knowing its first author's name and year should be enough.
So from the [top page of ADS](https://ui.adsabs.harvard.edu), first click on "First Author" and enter "Akhlaghi" in the requested place (so the search becomes ` author:"^Akhlaghi"`).
If you know the names of other co-authors, you can have a more effective search by also clicking on "Author" and entering their name (in this case, let's add `author:"Infante-Sainz"`).
The difference between the first author and the rest is a `^` before the name.
You can also limit your search by the published year by adding this extra term `year:2021`.

ADS also enables searching for datasets or software along side papers.
Therefore in the example above, after you do the search, you will see two entries: [2021ascl.soft06010A](https://ui.adsabs.harvard.edu/abs/2021ascl.soft06010A) and [2021CSE....23c..82A](https://ui.adsabs.harvard.edu/abs/2021CSE....23c..82A).
The first is not the paper, but the software!
You can tell by the `ascl.soft` in the name (which links to the software identifier provided by the [Astrophysics Source Code Library](https://ascl.net/) (or ASCL).
For other papers, it is usually datasets, they have a `yCat` string in their name.
Therefore, to cite the journal paper, we should go into the second one ([2021CSE....23c..82A](https://ui.adsabs.harvard.edu/abs/2021CSE....23c..82A)) with a journal name (`CSE`: for Computing in Science and Engineering), a volume and page number.

Once you have found your desired paper in ADS, you will be taken to a page like [this for Akhlaghi et al. 2021](https://ui.adsabs.harvard.edu/abs/2021CSE....23c..82A).
The last item in the left menu is "Export Citation".
When you click on it, you get to [a page like this](https://ui.adsabs.harvard.edu/abs/2021CSE....23c..82A/exportcitation).
Make sure that under "Select Export Format", it has written "BibTeX".
You can then click on "Copy to Clipboard" to copy the contents into your system's memory (to later paste into a plain-text editor).
You can now create a new file in your project called `references.tex` and put the copied contents in there.

This is the BibTeX format for publication data.
LaTeX will use any relevant part of this to construct the bibliography of your paper in what ever format you desire.
We will just make two changes to have the BibTeX entry below:
- An important part of the default entry that is recommended to change is the **key** (or the first word after `@ARTICLE{`).
You later use this "key" to cite this paper.
But the automatic key that ADS has put is not too human friendly and you will not remember this while writing your text.
It is better to change it to something more easier to remember, for example `akhlaghi21`, or `maneage` (since it about [Maneage](https://maneage.org)), or any name you are comfortable with like below.
- We will also replace the journal name (after `journal`) with a LaTeX macro (`\cise`).
This is done because by default, ADS puts the long name of journals like CiSE (that Akhlaghi. et al 2021 is published in).
But based on our bibliography format, we usually want the journal's short name.

```latex
@ARTICLE{akhlaghi21,
       author = {{Akhlaghi}, Mohammad and {Infante-Sainz}, Raul and {Roukema}, Boudewijn F. and {Khellat}, Mohammadreza and {Valls-Gabaud}, David and {Baena-Galle}, Roberto},
        title = "{Toward Long-Term and Archivable Reproducibility}",
      journal = {\cise},
     keywords = {Computer Science - Digital Libraries},
         year = 2021,
        month = may,
       volume = {23},
       number = {3},
        pages = {82-91},
          doi = {10.1109/MCSE.2021.3072860},
archivePrefix = {arXiv},
       eprint = {2006.03018},
 primaryClass = {cs.DL},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2021CSE....23c..82A},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```

The journal LaTeX macro is only one peculiarity with ADS bibliography.
For example, see the "Export Citation" of these papers: [2021A&A...654A..40T](https://ui.adsabs.harvard.edu/abs/2021A%26A...654A..40T) or [2020MNRAS.491.5317I](https://ui.adsabs.harvard.edu/abs/2020MNRAS.491.5317I).
If you look infront of `journal`, they don't have the journal name (A&A or MNRAS), but a macro called `\aap` and `\mnras`.
ADS does this so you can customize the journal name once as a macro, and have a similar format in any number of bibliography entries.
The full list of journal name macros [is defined in ADS](https://ui.adsabs.harvard.edu/help/actions/journal-macros).
So we'll define these macros in our preamble (for CiSE as well as A&A and MNRAS that publish many papers you will probably cite later!).

```latex
% ADS journal name macros
\newcommand{\aap}{A\&A}
\newcommand{\cise}{CiSE}
\newcommand{\mnras}{MNRAS}
```

To load BibLaTeX and specify the file containing the BibTeX information, you need to put the following lines in your preamble:
```latex
% Bibliography
\usepackage[natbib]{biblatex}
\addbibresource{references.tex}
```

The preamble is now ready.
To cite a paper in your text, you have to do the following steps within the body of your document (a fully working example follows):
* use `\citet{KEYNAME}` anywhere in the text that you want to cite the paper with a key called `KEYNAME` (`\citet{akhlaghi21}` in the example above).
    * You can also use `\citep{KEYNAME1, KEYNAME2}` to put the full citation(s) in paranthesis, not just the year.
* You then simply have to add `\printbibliography` anywhere in the body of your text that you want them to be printed.

```latex
\documentclass{article}

% To include graphics within the document.
\usepackage{graphicx}

% Set the size of the page and total width and height of text
\usepackage[a4paper, total={17cm,26cm}]{geometry}

% Custom macros of this project
\newcommand{\projecttitle}{My exciting project}
\newcommand{\projectauthors}{My name, My colleage}

% Clickable links to references and citations
\usepackage[ colorlinks,
             urlcolor=blue,
	     citecolor=blue,
	     linkcolor=blue,
             pdftitle={\projecttitle},
             pdfauthor={\projectauthors},
             pdfsubject={My first project with LaTeX},
             pdfkeywords={LaTeX, preparing reports}] {hyperref}

% First-page information.
\title{\projecttitle}
\author{\projectauthors}

% Include the analysis results as LaTeX macros
\input{results.tex}

% ADS journal name macros
\newcommand{\aap}{A\&A}
\newcommand{\cise}{CiSE}
\newcommand{\mnras}{MNRAS}

% Bibliography
\usepackage[natbib]{biblatex}
\addbibresource{references.tex}

% Start of printable content.
\begin{document}
\maketitle

\section{Introduction}
A star is an astronomical object comprising a luminous spheroid of plasma held together by its gravity.
The nearest star to Earth is the Sun.
Many other stars are visible to the naked eye at night, but their immense distances from Earth make them appear as fixed points of light.
See Section \ref{sec:star-birth}.

\begin{equation}
  \label{eq:magnitude}
  m-m_r=-2.5\times log_{10}\left(\frac{B}{B_r}\right)
\end{equation}

\section{A star is born}
\label{sec:star-birth}
A star's life begins with the gravitational collapse of a gaseous nebula of material composed primarily of hydrogen, along with helium and trace amounts of heavier elements.
See equation \ref{eq:magnitude} for the definition of magnitude
Its total mass is the main factor determining its evolution and eventual fate.
Figure \ref{fig:life-cycle} in page \pageref{fig:life-cycle} shows the life-cycle of a star.
Within a radius of $\projectrad$ degrees around the coordinates ($\projectra,\projectdec$), we found $\projectnumq$ stars in Gaia {\projectdataset}.
See \citet{akhlaghi21} for conducting and publishing a reproducible research.

% Life-cycle of star, from Wikipedia
% Image from: https://en.wikipedia.org/wiki/Star#/media/File:Star_life_cycles_red_dwarf_en.svg
\begin{figure}[!b]
  \begin{center}
    \includegraphics[width=0.8\linewidth]{img/star-life.png}
  \end{center}
  \caption{\label{fig:life-cycle} Life cycle of a star.}
\end{figure}

\subsection{Chemical aspects}
Stellar nucleosynthesis in stars or their remnants creates almost all naturally occurring chemical elements heavier than lithium.
Stellar mass loss or supernova explosions return chemically enriched material to the interstellar medium.
They are then recycled into new stars.

% Create the bibliography
\printbibliography

% End of printable content.
\end{document}
```

If you simply run `pdflatex report.tex`, you won't see any citation inserted.
This is because BibLaTeX's special software `biber` needs to be run in between two LaTeX runs.
So you should compile the PDF with these three commands:
```shell
pdflatex report.tex
biber report
pdflatex report.tex
```

You now see that the paper is cited as `Akhlaghi et al. [1]`, and a new "References" section has been added.
Until the next time you add a reference (or modify and existing one), you won't need to run `biber` anymore and can simply run `pdflatex` once like before.
Any time you make a change in `references.tex`, you should re-run `biber`.

But the default BibLaTeX bibliography is too complete!
Things like the title, the month and several other things are extra, making it too long.
Most journals in astrophysics don't include so much information in bibliography!
Like the `hyperref` package above, you have many configuration options, let's have a look at a common set to make it look like the styles of bibliography in astronomical journals:

```latex
% Bibliography
\usepackage[ natbib,
             url=false,
             dashed=false,
             eprint=false,
             maxbibnames=2,
             minbibnames=1,
             hyperref=true,
	     backend=biber,
             maxcitenames=2,
             mincitenames=1,
             giveninits=true,
             style=authoryear,
             uniquelist=false ]{biblatex}

% Bibliography: where to get the information
\addbibresource{references.tex}

% Bibliography: Remove the "In:", month and title
\renewbibmacro{in:}{}
\AtEveryBibitem{\clearfield{month}}
\AtEveryBibitem{\clearfield{title}}
```

But generally, you don't have to worry about these style issues.
Journals already define their own styles, and by simply loading their styles, your article will automatically follow their required standards!
The goal here is to create a stand-alone (independent of any journal) LaTeX PDF, to show you how the journals do it ;-).

As an **exercise**, try adding citations to some of the latest papers that you have read.





### Keeping `report.tex` clean

As you may have noticed, as you are adding more and more features to your report, the preamble is gradually becoming more and more crowded!
This makes it hard for your to focus on your content (one promise of LaTeX)!

Fortunately there is a very elegant and nice solution to this problem.
It uses one of the features that we have already mentioned before: `\input{}`.
We will simply move the preamble into a newly created file called `preamble.tex`:

```latex
%% Contents of 'preamble.tex':
% To include graphics within the document.
\usepackage{graphicx}

% Set the size of the page and total width and height of text
\usepackage[a4paper, total={17cm,26cm}]{geometry}

% Clickable links to references and citations
\usepackage[ colorlinks,
             urlcolor=blue,
	     citecolor=blue,
	     linkcolor=blue,
             pdftitle={\projecttitle},
             pdfauthor={\projectauthors},
             pdfsubject={My first project with LaTeX},
             pdfkeywords={LaTeX, preparing reports}] {hyperref}

% First-page information.
\title{\projecttitle}
\author{\projectauthors}

% Include the analysis results as LaTeX macros
\input{results.tex}

% ADS journal name macros
\newcommand{\aap}{A\&A}
\newcommand{\cise}{CiSE}
\newcommand{\mnras}{MNRAS}

% Bibliography
\usepackage[ natbib,
             url=false,
             dashed=false,
             eprint=false,
             maxbibnames=2,
             minbibnames=1,
             hyperref=true,
	     backend=biber,
             maxcitenames=2,
             mincitenames=1,
             giveninits=true,
             style=authoryear,
             uniquelist=false ]{biblatex}

% Bibliography: where to get the information
\addbibresource{references.tex}

% Bibliography: Remove the "In:", month and title
\renewbibmacro{in:}{}
\AtEveryBibitem{\clearfield{month}}
\AtEveryBibitem{\clearfield{title}}
```

We can now simply include this file in the main report's source like you see below!
Do you see how all that complexity of the different packages and your various settings have now been put another file?
You can now happily focus on the contents of your exciting scientific discovery:

```latex
\documentclass{article}

% Project title and authors.
\newcommand{\projecttitle}{My exciting project}
\newcommand{\projectauthors}{My name, My colleage}

% Preamble is in another file.
\input{preamble.tex}

% Start of printable content.
\begin{document}
\maketitle

\section{Introduction}
A star is an astronomical object comprising a luminous spheroid of plasma held together by its gravity.
The nearest star to Earth is the Sun.
Many other stars are visible to the naked eye at night, but their immense distances from Earth make them appear as fixed points of light.
See Section \ref{sec:star-birth}.

\begin{equation}
  \label{eq:magnitude}
  m-m_r=-2.5\times log_{10}\left(\frac{B}{B_r}\right)
\end{equation}

\section{A star is born}
\label{sec:star-birth}
A star's life begins with the gravitational collapse of a gaseous nebula of material composed primarily of hydrogen, along with helium and trace amounts of heavier elements.
See equation \ref{eq:magnitude} for the definition of magnitude
Its total mass is the main factor determining its evolution and eventual fate.
Figure \ref{fig:life-cycle} in page \pageref{fig:life-cycle} shows the life-cycle of a star.
Within a radius of $\projectrad$ degrees around the coordinates ($\projectra,\projectdec$), we found $\projectnumq$ stars in Gaia {\projectdataset}.
See \citet{akhlaghi21} for conducting and publishing a reproducible research.

% Life-cycle of star, from Wikipedia
% Image from: https://en.wikipedia.org/wiki/Star#/media/File:Star_life_cycles_red_dwarf_en.svg
\begin{figure}[!b]
  \begin{center}
    \includegraphics[width=0.8\linewidth]{img/star-life.png}
  \end{center}
  \caption{\label{fig:life-cycle} Life cycle of a star.}
\end{figure}

\subsection{Chemical aspects}
Stellar nucleosynthesis in stars or their remnants creates almost all naturally occurring chemical elements heavier than lithium.
Stellar mass loss or supernova explosions return chemically enriched material to the interstellar medium.
They are then recycled into new stars.

% Create the bibliography
\printbibliography

% End of printable content.
\end{document}
```

In the example above, we kept the title and author information (that should have been in the preamble) in `report.tex`.
Of course, you can also put those in `preamble.tex`, but since they are also directly printed on the final PDF, and it feels good to see your name and title at the start of your LaTeX source, we have kept them in `report.tex`.





### Keeping the top directory clean

After the commands of the previous section, you will notice that your top directory has become very crowded (see the listing below!).
Which one of these did you write by hand (and are valuable), and which ones were automatically generated (not valuable: can always be reproduced)?

```shell
$ ls
analysis.sh     preamble.tex    report.bbl  report.log  report.run.xml
from-gaia.fits  references.tex  report.bcf  report.out  report.tex
img             report.aux      report.blg  report.pdf  results.tex
```

Preparations for bibliography in particular, produce a lot of temporary files that can pollute your top-level directory.
You may mistakenly delete a valuable hand-written file instead of a temporary file!
Fortunately there is a good solution.
But first, let's clean up the directory from all the files we didn't create by hand:

```shell
# Delete all files not created by hand
rm -r report.bbl report.blg report.out report.run.xml report.aux \
      report.bcf  report.log  report.pdf from-gaia.fits results.tex img/

# Only the hand-written files (valuable) remain:
$ ls
analysis.sh  preamble.tex  references.tex  report.tex
```

To run `pdflatex`, we first need to download the image `star-life.png` and produce `results.tex` which will host the LaTeX macros containing our analysis results.
One core principle to keep our top-level source directory clean is to make a single `build` directory and put all the outputs there:

```shell
$ mkdir build
```

#### Script to download the image

To automate the download of `star-life.png`, let's make a script for it and call it `download-img.sh` (with the following content):

```shell
#!/bin/bash

# Basic directories:
bdir=build
bimgdir=$bdir/img

# Make sure the directory for the image exists:
if ! [ -d $bimgdir ]; then mkdir $bimgdir; fi

# Download the image.
wget -O $bimgdir/star-life.png \
     https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Star_life_cycles_red_dwarf_en.svg/640px-Star_life_cycles_red_dwarf_en.svg.png
```

We can now make it executable, run it, and have a look in the build directory to see that it has indeed

```shell
$ chmod +x download-img.sh
$ ./download-img.sh
```

#### Script to do the analysis

We have already written a basic analysis script above.
To have a clean directory structure, we just need to write its temporary file (downloaded from Gaia) in `build/catalogs/gaia.fits` and the final LaTeX macros file to `build/tex/results.tex`.
So at the start of the script, we'll make sure the directories already exist, and then do the analysis.
Below you can see the updated contents of `analysis.sh`:

```shell
#!/bin/bash

# Parameters
rad=0.1
dataset=dr3
ra=53.15958
dec=-27.78191

# Directories
bdir=build
btexdir=$bdir/tex
bcatdir=$bdir/catalogs

# If the catalogs or TeX directory doesn't exist, make it.
if ! [ -d $bcatdir ]; then mkdir $bcatdir; fi
if ! [ -d $btexdir ]; then mkdir $btexdir; fi

# Query the Gaia database.
astquery gaia --dataset=gaia$dataset.gaia_source --center=$ra,$dec \
	 --radius=$rad --output=$bcatdir/gaia.fits

# Find the number of objects that were downloaded
numq=$(asttable $bcatdir/gaia.fits | wc -l)

# Convert the dataset name to upper-case (which is common in English
# writing). This is a GNU Bash feature (a single '^' will only make
# the first character upper-case, while '^^' will affect the wole
# word).
datasetup=${dataset^^}

# Write inputs and outputs as LaTeX macros:
results=$btexdir/results.tex
printf '\\'newcommand{'\\'projectra}{$ra}"\n"              > $results
printf '\\'newcommand{'\\'projectdec}{$dec}"\n"           >> $results
printf '\\'newcommand{'\\'projectrad}{$rad}"\n"           >> $results
printf '\\'newcommand{'\\'projectnumq}{$numq}"\n"         >> $results
printf '\\'newcommand{'\\'projectdataset}{$datasetup}"\n" >> $results
```

Again, you can make it executable and run it:

```shell
$ chmod +x ./analysis.sh
$ ./analysis.sh
```

#### Correcting the LaTeX sources

Since we have changed the directories of the inputs to LaTeX, we need to make some corrections in the main body and preamble.
In the main report, we should just change the location of the image:

```latex
%% Contents of report.tex
\documentclass{article}

% Project title and authors.
\newcommand{\projecttitle}{My exciting project}
\newcommand{\projectauthors}{My name, My colleage}

% Preamble is in another file.
\input{preamble.tex}

% Start of printable content.
\begin{document}
\maketitle

\section{Introduction}
A star is an astronomical object comprising a luminous spheroid of plasma held together by its gravity.
The nearest star to Earth is the Sun.
Many other stars are visible to the naked eye at night, but their immense distances from Earth make them appear as fixed points of light.
See Section \ref{sec:star-birth}.

\begin{equation}
  \label{eq:magnitude}
  m-m_r=-2.5\times log_{10}\left(\frac{B}{B_r}\right)
\end{equation}

\section{A star is born}
\label{sec:star-birth}
A star's life begins with the gravitational collapse of a gaseous nebula of material composed primarily of hydrogen, along with helium and trace amounts of heavier elements.
See equation \ref{eq:magnitude} for the definition of magnitude
Its total mass is the main factor determining its evolution and eventual fate.
Figure \ref{fig:life-cycle} in page \pageref{fig:life-cycle} shows the life-cycle of a star.
Within a radius of $\projectrad$ degrees around the coordinates ($\projectra,\projectdec$), we found $\projectnumq$ stars in Gaia {\projectdataset}.
See \citet{akhlaghi21} for conducting and publishing a reproducible research.

% Life-cycle of star, from Wikipedia
% Image from: https://en.wikipedia.org/wiki/Star#/media/File:Star_life_cycles_red_dwarf_en.svg
\begin{figure}[!b]
  \begin{center}
    \includegraphics[width=0.8\linewidth]{build/img/star-life.png}
  \end{center}
  \caption{\label{fig:life-cycle} Life cycle of a star.}
\end{figure}

\subsection{Chemical aspects}
Stellar nucleosynthesis in stars or their remnants creates almost all naturally occurring chemical elements heavier than lithium.
Stellar mass loss or supernova explosions return chemically enriched material to the interstellar medium.
They are then recycled into new stars.

% Create the bibliography
\printbibliography

% End of printable content.
\end{document}
```

In the preamble, we should change the location of `results.tex`:

```latex
% To include graphics within the document.
\usepackage{graphicx}

% Set the size of the page and total width and height of text
\usepackage[a4paper, total={17cm,26cm}]{geometry}

% Clickable links to references and citations
\usepackage[ colorlinks,
             urlcolor=blue,
	     citecolor=blue,
	     linkcolor=blue,
             pdftitle={\projecttitle},
             pdfauthor={\projectauthors},
             pdfsubject={My first project with LaTeX},
             pdfkeywords={LaTeX, preparing reports}] {hyperref}

% First-page information.
\title{\projecttitle}
\author{\projectauthors}

% Include the analysis results as LaTeX macros
\input{build/tex/results.tex}

% ADS journal name macros
\newcommand{\aap}{A\&A}
\newcommand{\cise}{CiSE}
\newcommand{\mnras}{MNRAS}

% Bibliography
\usepackage[ url=false,
             dashed=false,
             eprint=false,
             maxbibnames=2,
             minbibnames=1,
             hyperref=true,
             maxcitenames=2,
             mincitenames=1,
             giveninits=true,
             style=authoryear,
             uniquelist=false,
             backend=biber,natbib]{biblatex}

% Bibliography: where to get the information
\addbibresource{references.tex}

% Bibliography: Remove the "In:", month and title
\renewbibmacro{in:}{}
\AtEveryBibitem{\clearfield{month}}
\AtEveryBibitem{\clearfield{title}}
```

#### Script to build the paper

We finally have all the components to build the paper!
But one of the main creators of temporary files were `pdflatex` and `biber`.
Fortunately they both have an  `--output-directory` option to let you specify where the outputs should be written to.

The `--output-directory` option takes the address of an existing directory, and both programs will write their products there (instead of the current directory).
Therefore, we first need to create the directory before using it.
We'll instruct both `pdflatex` and `biber` to write/read temporary files from the `build/tex` directory that we have already created in the analysis script before this.
We'll also use the `--halt-on-error` option of `pdflatex` so if an error occurs, it doesn't wait for your interactive input, and it just aborts.

Having all these temporary output files in a separate directory greatly helps in managing the valuable files that you have written by hand!
But one output file is important for you: the final PDF!
So we'll finish this script by making a symbolic link to it in the top directory!
In this way, any update to `report.pdf` will always be visible in the symbolic link.

The final script can be seen below (let's call it `create-pdf.sh`):

```shell
#!/bin/bash
pdflatex --output-directory=build/tex --halt-on-error report.tex
biber    --output-directory=build/tex report
pdflatex --output-directory=build/tex --halt-on-error report.tex
ln -s build/tex/report.pdf ./
```

As before, you should just make the script executable and run it:

```shell
$ chmod +x create-pdf.sh
$ ./create-pdf.sh
```

#### Final directory structure

If you get a listing of the contents of the directory, you will see that none of the temporary files are present in the top source directory any more:
As expected, they are all in the `build/` directory and except for that directory, all the other files in the top project directory are hand-written:

```shell
$ ls -R ./
./:
analysis.sh  create-pdf.sh    preamble.tex    report.pdf
build        download-img.sh  references.tex  report.tex

./build:
catalogs  img  tex

./build/catalogs:
gaia.fits

./build/img:
star-life.png

./build/tex:
report.aux  report.bcf  report.log  report.pdf      results.tex
report.bbl  report.blg  report.out  report.run.xml
```


#### Cleaning the directories and reproducing the PDF

Since all the built products are in the `build/` directory, to completely remove all built products, you can simply delete that:

```shell
$ rm -r build
```

To reproduce your PDF (and your analysis!), you need to create the build directory, run the three scripts like below:

```shell
$ mkdir build
$ ./download-img.sh
$ ./analysis.sh
$ ./create-pdf.sh
```




### Makefiles to manage the LaTeX commands

We have made significant progress in having a clean "source" directory (where you keep the valuable files that you have written by hand), and "build" directory, which will contain the analysis and LaTeX outputs.
Within the source, the text of your report is now nicely separated from the increasingly complex preamble, letting you focus on your writing :-).

Having each group of commands in one script (like the example above) is good, but still prone to errors:
- Breaking each small set of commands into one separate shell script will soon result in many small shell scripts that can be hard to manage.
- Your project will grow, and you may forget to run one of the scripts, or which one should be run before/after the other.
- Each time a script is run, all parts will be re-run, even if you only need a small part of it.

Fortunately there is one final solution to also solve these problems and make your research even easier!
It is Make (that we introduced in [SMACK 14 we introduced Make](smack-14-make.md), its [recording is also avialable](https://www.youtube.com/watch?v=65EhAL08Mb8)).

Below, you can see the `Makefile` that will simplify the creation of your paper/report.

```make
# Parameters
rad=0.1
dataset=dr2
ra=53.15958
dec=-27.78191

# Top-level directories
bdir=build

# Define default target, and basic Make settings.
all: report.pdf
.ONESHELL:
SHELL = /bin/bash

# Building (and cleaning!) the top directory:
$(bdir):; mkdir $@
clean:; rm -r $(bdir)

# Sub-directories of 'build'
bimgdir=$(bdir)/img
btexdir=$(bdir)/tex
bcatdir=$(bdir)/catalog
$(btexdir) $(bimgdir) $(bcatdir): | $(bdir); mkdir $@



# Download image showing star's life cycle
starlife = $(bimgdir)/star-life.png
$(starlife): | $(bimgdir)
	wget -O $@ \
	     https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Star_life_cycles_red_dwarf_en.svg/640px-Star_life_cycles_red_dwarf_en.svg.png



# Download Gaia's catalog for the requested coordinates.
gaia = $(bcatdir)/gaia.fits
$(gaia): | $(bcatdir)
	astquery gaia --dataset=gaia$(dataset).gaia_source \
	         --center=$(ra),$(dec) --radius=$(rad) --output=$@



# Write all analysis results into 'results.tex'.
results = $(btexdir)/results.tex
$(results): $(starlife) $(gaia) | $(btexdir)

#	Write the inputs to the project.
	printf '\\'newcommand{'\\'projectra}{$(ra)}"\n"    > $@
	printf '\\'newcommand{'\\'projectdec}{$(dec)}"\n" >> $@
	printf '\\'newcommand{'\\'projectrad}{$(rad)}"\n" >> $@

#	Find the number of objects downloaded from Gaia.
	numq=$$(asttable $$gaia | wc -l)
	printf '\\'newcommand{'\\'projectnumq}{$$numq}"\n" >> $@

#	Convert the dataset name to upper-case (which is common in
#	English writing). This is a GNU Bash feature (a single '^'
#	will only make the first character upper-case, while '^^' will
#	affect the wole word).
	datasetup=$${dataset^^}
	printf '\\'newcommand{'\\'projectdataset}{$$datasetup}"\n" >> $@



# Bibliography (but first: a draft of the PDF). Note that the
# bibliography depends on 'references.tex'. So the bibliography will
# only be re-generated if you update your references :-).
bbl=$(btexdir)/report.bbl
$(bbl): references.tex | $(btexdir) $(results) $(starlife)
	pdflatex --output-directory=build/tex --halt-on-error report.tex
	biber    --output-directory=build/tex report



# Final PDF
report.pdf: report.tex $(bbl) $(results) $(starlife)
	pdflatex --output-directory=build/tex --halt-on-error report.tex
	ln -sf $(btexdir)/report.pdf ./
```

To produce your PDF from scratch, you can simply run the simple `make` command!
You don't have to worry about any of the details of the commands, their order, or their options!

```shell
$ make
```

You can also easily "clean" all the built products with the command below, and not have to worry at all about where the built products are!

```shell
$ make clean
```

As you saw above, the contents of the three shell scripts have now been taken inside this Makefile.
Note that we done the two analysis phases (downloading the image from Wikipedia, and querying the Gaia database) as separate rules.
These two rules were set as a prerequisite of `results.tex`, which is the bottle-neck between the analysis phase of the project and the PDF production phase.
The `results.tex` is then the prerequisite of the PDF production phase.
Therefore, all your final analysis outputs should ultimately (possibly through other targets) be a prerequisite of `results.tex`, not the PDF!

With this structure, the following optimizations will be enabled through Make: greatly simplifying your work and letting you focus on your exciting science, instead of mundane technicalities.
- The bibliography will be re-created **only when** `references.tex` is updated.
- The analysis will be re-done **only when** you delete `build/tex/results.tex`.
- The input image will be re-downloaded **only when** you delete `build/img/star-life.png`.
- As your project grows and more steps are added, you can simply run `make -j8` to run the independent steps in parallel and greatly speed up your work.

You can safely delete the three shell scripts now and make your top-level project directory even cleaner:

```shell
rm analysis.sh download-img.sh create-pdf.sh
```

Besides the `build` and `report.pdf`, your top source directory now only has four files (three of them are LaTeX!):

```shell
$ ls
build  Makefile  preamble.tex  references.tex  report.pdf  report.tex
```

This will let you focus on writing contents of your exciting scientific research, and not have to worry about the low-level details, unless some change is necessary.




### Installing LaTeX

### Installing packages
